<?php

namespace Esprit\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UtilisateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
         ->add('login',        'text', array(
                'label' => 'login du collaborateur',
                'attr' => array(
                    'class' => 'input-large',
                    'placeholder' => 'entrez votre login',
                )
            ))
                 ->add('password',        'text', array(
                'label' => 'password du collaborateur',
                'attr' => array(
                    'class' => 'input-large',
                    'placeholder' => 'entrez le password',
                )
            ))
              ->add('Nom',        'text', array(
                'label' => 'Nom de Collaborateur',
                'attr' => array(
                    'class' => 'input-large',
                    'placeholder' => 'entrez votre Nom',
                )
            ))
            ->add('prenom',       'text', array(
                'label' => 'Prenom de Collaborateur',
                'attr' => array(
                    'class' => 'input-large',
                    'placeholder' => 'entrez votre Prenom',
                    )
                ))
            ->add('dateNaissance','birthday')
             ->add('telephone',     'text', array(
                'attr' => array(
                    'class' => 'input-large',
                    'placeholder' => 'entrez votre Nummero ',
                    )
                ))
            ->add('adresse','textarea')
           ->add('email',        'email', array(
                'attr' => array(
                    'class' => 'input-large',
                    'placeholder' => 'entrez votre Adresse Email',
                    )
                ))
            ->add('date','date')
            ->add('image',        new ImageType()) // Rajoutez cette ligne
            ->add('service', 'entity', array(
                  'class'    => 'EspritAdminBundle:Service',
                  'property' => 'nom'))  
           
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Esprit\AdminBundle\Entity\Collaborateur'
        ));
    }

    public function getName()
    {
        return 'esprit_adminbundle_collaborateurtype';
    }

  

}
