<?php

namespace SmartBook\LecteurBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReclamationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',        'text', array(
                'label' => 'Objet',
                'attr' => array(
                    'class' => 'input-large',
                    
                )
            ))
            ->add('contenu','textarea')
           
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SmartBook\LecteurBundle\Entity\Reclamation'
        ));
    }

    public function getName()
    {
        return 'esprit_adminbundle_reclamationtype';
    }
}
