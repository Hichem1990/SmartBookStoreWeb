<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PublicationForm
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Form;
 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use SmartBook\LecteurBundle\Form\LivreForm;
use SmartBook\LecteurBundle\Entity\Livre;

class PublicationForm extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder,array $options)
    {
        
           $builder
                ->add('titre','text')
                ->add('presentation','textarea')              
                ->add('Livre',new LivreForm())
            ;
       
    }
    public function getName()
    {
        return "annonce" ;
    }
    public function getDefaultOptions(
    \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver
) {
    $resolver->setDefaults(array(
        'data_class' => 'SmartBookLecteurBundle\Entity\Livre',
    ));
}
   
    
}
