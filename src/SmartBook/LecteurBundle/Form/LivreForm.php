<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LivreForm
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Form;
 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LivreForm extends AbstractType {
    
   public function buildForm(FormBuilderInterface $builder,array $options)
    {
      $builder
       ->add('nomLivre','text')
       ->add('auteur','text')
       ->add('Langue', 'choice', array(
        'choices'  => array('Français' => 'francais','Anglais' => 'anglais',
         'Arabe' => 'arabe'),
        'required' => true,
        ))
      
       ->add('Prix','integer')
       ->add('nbrPages','integer')
       ->add('dateEdition','date') 
       ->add('etat','text')
       ->add('categorie', 'choice', array(
        'choices'  => array('Politique' => 'politique','Cuisine' => 'cuisine',
         'Sport' => 'sport','Art' => 'art','Sport'=>'sport'),
        'required' => true,
        ))
       ->add('imageL','file')
      ->add('Publier','submit')
      ->add('Annuler','reset');
    }
    public function getName()
    {
        return "annonce" ;
    }
    public function getDefaultOptions(OptionsResolverInterface $resolver) {
    $resolver->setDefaults(array(
    'data_class' => "SmartBookLecteurBundle\Entity\Livre", ));
}
      
}