<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EvenementRepository
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Entity;
use Doctrine\ORM\EntityRepository ;
class EvenementRepository  extends EntityRepository  {
    
    public function findByUtilisateur($id)
    {
        $query=$this->getEntityManager()->createQuery("SELECT e from SmartBookLecteurBundle:Evenement e"
       . "JOIN ¨SmartBookLecteurBundle:Utilisateur u WHERE e.utilisateur.id=u.id AND u.id=:utilisateur")
        ->setParameter("utilisateur",$id);
        return $query->getResult();
    }
}
