<?php
// src/Acme/UserBundle/Entity/User.php

namespace SmartBook\LecteurBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="utilisateur")
 */
class Utilisateur extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=false)
     */
    private $nom; 
    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=30, nullable=false)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=10, nullable=false)
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=1000, nullable=false)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=100, nullable=false)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="num_tel", type="string", length=12, nullable=false)
     */
    private $numTel;

    /**
     * @var string
     *
     * @ORM\Column(name="date_naissance", type="string", length=10, nullable=false)
     */
    private $dateNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="etat_compte", type="string", length=30, nullable=false)
     */
    private $etatCompte = 'actif';

    /**
     * @var string
     *
     * @ORM\Column(name="etat_inscription", type="string", length=30, nullable=false)
     */
    private $etatInscription = 'non valide';

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_signale", type="integer", nullable=false)
     */
    private $nbrSignale = '0';
    
    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getPrenom() {
        return $this->prenom;
    }

    function getSexe() {
        return $this->sexe;
    }

    function getImage() {
        return $this->image;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getNumTel() {
        return $this->numTel;
    }

    function getDateNaissance() {
        return $this->dateNaissance;
    }

    function getLogin() {
        return $this->login;
    }

    function getMdp() {
        return $this->mdp;
    }

    function getEtatCompte() {
        return $this->etatCompte;
    }

    function getEtatInscription() {
        return $this->etatInscription;
    }

    function getNbrSignale() {
        return $this->nbrSignale;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    function setSexe($sexe) {
        $this->sexe = $sexe;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setNumTel($numTel) {
        $this->numTel = $numTel;
    }

    function setDateNaissance($dateNaissance) {
        $this->dateNaissance = $dateNaissance;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setMdp($mdp) {
        $this->mdp = $mdp;
    }

    function setEtatCompte($etatCompte) {
        $this->etatCompte = $etatCompte;
    }

    function setEtatInscription($etatInscription) {
        $this->etatInscription = $etatInscription;
    }

    function setNbrSignale($nbrSignale) {
        $this->nbrSignale = $nbrSignale;
    }

        public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    function getId_u() {
        return $this->id_u;
    }

    

    function setId_u($id_u) {
        $this->id_u = $id_u;
    }

    


}