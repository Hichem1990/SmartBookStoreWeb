<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UtilisateurRepository
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Entity;
use Doctrine\ORM\EntityRepository ;
class UtilisateurRepository extends EntityRepository  {
    
    public function findByRoles($role)
    {
        $query=$this->getEntityManager()->createQuery("SELECT u from SmartBookLecteurBundle:User u"
       . "WHERE LIKE u.roles=:roles")
        ->setParameter("roles",$role);
        return $query->getResult();
    }
    
}
