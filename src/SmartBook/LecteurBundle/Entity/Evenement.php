<?php

namespace SmartBook\LecteurBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evenement
 *
 * @ORM\Table(name="evenement", indexes={@ORM\Index(name="utilisateur", columns={"utilisateur"})})
 * @ORM\Entity
 */
class Evenement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_e", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idE;

    /**
     * @var string
     *
     * @ORM\Column(name="date_event", type="string", length=10, nullable=false)
     */
    private $dateEvent;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=30, nullable=false)
     */
    private $titre;

      /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="utilisateur", referencedColumnName="id")
     * })
     */
    private $utilisateur;

    /**
     * @var integer
     *
     * @ORM\Column(name="Durée_Eve", type="integer", nullable=false)
     */
    private $dureeEve;

    /**
     * @var string
     *
     * @ORM\Column(name="Lieu_Eve", type="string", length=100, nullable=false)
     */
    private $lieuEve;

    /**
     * @var string
     *
     * @ORM\Column(name="Description_Eve", type="string", length=1000, nullable=false)
     */
    private $descriptionEve;

    /**
     * @var string
     *
     * @ORM\Column(name="Image", type="string", length=100, nullable=false)
     */
    private $image;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_participant", type="integer", nullable=false)
     */
    private $nbrParticipant = '0';

    function getIdE() {
        return $this->idE;
    }

    function getDateEvent() {
        return $this->dateEvent;
    }

    function getTitre() {
        return $this->titre;
    }

    function getUtilisateur() {
        return $this->utilisateur;
    }

    function getDureeEve() {
        return $this->dureeEve;
    }

    function getLieuEve() {
        return $this->lieuEve;
    }

    function getDescriptionEve() {
        return $this->descriptionEve;
    }

    function getImage() {
        return $this->image;
    }

    function getNbrParticipant() {
        return $this->nbrParticipant;
    }

    function setIdE($idE) {
        $this->idE = $idE;
    }

    function setDateEvent($dateEvent) {
        $this->dateEvent = $dateEvent;
    }

    function setTitre($titre) {
        $this->titre = $titre;
    }

    function setUtilisateur(Utilisateur $utilisateur) {
        $this->utilisateur = $utilisateur;
    }

    function setDureeEve($dureeEve) {
        $this->dureeEve = $dureeEve;
    }

    function setLieuEve($lieuEve) {
        $this->lieuEve = $lieuEve;
    }

    function setDescriptionEve($descriptionEve) {
        $this->descriptionEve = $descriptionEve;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setNbrParticipant($nbrParticipant) {
        $this->nbrParticipant = $nbrParticipant;
    }


}
