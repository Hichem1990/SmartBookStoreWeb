<?php

namespace SmartBook\LecteurBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Livre
 *
 * @ORM\Table(name="livre", indexes={@ORM\Index(name="libraire", columns={"libraire"}), @ORM\Index(name="panier", columns={"panier"})})
 * @ORM\Entity
 */
class Livre
{
    
    function getIdL() {
        return $this->idL;
    }

    function getNomLivre() {
        return $this->nomLivre;
    }

    function getImageL() {
        return $this->imageL;
    }

    function getAuteur() {
        return $this->auteur;
    }

    function getLangue() {
        return $this->langue;
    }

    function getPrix() {
        return $this->prix;
    }

    function getNbrPages() {
        return $this->nbrPages;
    }

    function getDateEdition() {
        return $this->dateEdition;
    }

    function getEtat() {
        return $this->etat;
    }

    function getCategorie() {
        return $this->categorie;
    }

    function getNbrAime() {
        return $this->nbrAime;
    }

    function getLibraire() {
        return $this->libraire;
    }

    function getPanier() {
        return $this->panier;
    }

    function setIdL($idL) {
        $this->idL = $idL;
    }

    function setNomLivre($nomLivre) {
        $this->nomLivre = $nomLivre;
    }

    function setImageL($imageL) {
        $this->imageL = $imageL;
    }

    function setAuteur($auteur) {
        $this->auteur = $auteur;
    }

    function setLangue($langue) {
        $this->langue = $langue;
    }

    function setPrix($prix) {
        $this->prix = $prix;
    }

    function setNbrPages($nbrPages) {
        $this->nbrPages = $nbrPages;
    }

    function setDateEdition($dateEdition) {
        $this->dateEdition = $dateEdition;
    }

    function setEtat($etat) {
        $this->etat = $etat;
    }

    function setCategorie($categorie) {
        $this->categorie = $categorie;
    }

    function setNbrAime($nbrAime) {
        $this->nbrAime = $nbrAime;
    }

    function setLibraire($libraire) {
        $this->libraire = $libraire;
    }

    function setPanier($panier) {
        $this->panier = $panier;
    }

        /**
     * @var integer
     *
     * @ORM\Column(name="id_l", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idL;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_livre", type="string", length=50, nullable=false)
     */
    private $nomLivre;

    /**
     * @var string
     *
     * @ORM\Column(name="image_l", type="string", length=100, nullable=false)
     */
    private $imageL;

    /**
     * @var string
     *
     * @ORM\Column(name="auteur", type="string", length=50, nullable=false)
     */
    private $auteur;

    /**
     * @var string
     *
     * @ORM\Column(name="langue", type="string", length=30, nullable=false)
     */
    private $langue;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_pages", type="integer", nullable=false)
     */
    private $nbrPages;

    /**
     * @var string
     *
     * @ORM\Column(name="date_edition", type="string", length=10, nullable=false)
     */
    private $dateEdition;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=30, nullable=false)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=100, nullable=false)
     */
    private $categorie;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_aime", type="integer", nullable=false)
     */
    private $nbrAime = '0';

      /**
     * @var \Libraire   *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="libraire", referencedColumnName="id")
     * })
     */
     
    private $libraire;

    /**
     * @var integer
     *
     * @ORM\Column(name="panier", type="integer", nullable=false)
     */
    private $panier;
    /*     * ***********   *****************   ******* */
    /*     * ***********   START UPLOAD FILE   ******* */
    /*     * ***********   *****************   ******* */

    public function getAbsulatePath() {
        return $this->imageL === null ?
                null :
                $this->getUploadRootDir()
                . '/' .
                $this->imageL;
    }

    public function getWebPath() {
        return $this->imageL === null ?
                null :
                $this->getUploadDir() . '/' .
                $this->imageL;
    }

    /**
     * le chemin absolu du répertoire où les documents uploadés doivent
     * être sauvegardés
     * @return string
     */
    protected function getUploadRootDir() {
        // le chemin absolu du répertoire où les documents uploadés doivent
        // être sauvegardés
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    /**
     * on se débarrasse de « __DIR__ » afin de ne pas avoir de 
     * problème lorsqu'on affiche le document/image dans la vue.
     * @return string
     */
    protected function getUploadDir() {
        // on se débarrasse de « __DIR__ » afin de ne pas avoir de problème
        // lorsqu'on affiche le document/image dans la vue.
        return 'uploads/livres';
    }
    private $file;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    public function upload()
    {  
    if (null === $this->getFile()) {
        return;
    } 
    $this->getFile()->move(
        $this->getUploadRootDir(),
        $this->imageL
    );
    $this->imageL = $this->getFile()->getClientOriginalName();
    $this->file = null;
}
   

}
