<?php

namespace SmartBook\LecteurBundle\Entity;
use SmartBook\LecteurBundle\Entity\Livre;
use SmartBook\LecteurBundle\Entity\Utilisateur;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Publication
 *
 *@ORM\Entity(repositoryClass="SmartBook\LecteurBundle\Entity\PublicationRepository") 
 * @ORM\Table(name="publication", indexes={@ORM\Index(name="utilisateur", columns={"utilisateur"}), @ORM\Index(name="livre", columns={"livre"})})
 * @ORM\Entity
 */
class Publication
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_p", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    
    
    private $idP;

    /**
     * @var string
     *
     * @ORM\Column(name="date_pub", type="string", length=10, nullable=false)
     */
    private $datePub;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=30, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="presentation", type="text", nullable=false)
     */
    private $presentation;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="utilisateur", referencedColumnName="id")
     * })
     */
    private $utilisateur;
    
    function getIdP() {
        return $this->idP;
    }

    function getDatePub() {
        return $this->datePub;
    }

    function getTitre() {
        return $this->titre;
    }

    function getType() {
        return $this->type;
    }

    function getPresentation() {
        return $this->presentation;
    }

    function getUtilisateur() {
        return $this->utilisateur;
    }

    function getLivre() {
        return $this->livre;
    }

    function setIdP($idP) {
        $this->idP = $idP;
    }

    function setDatePub($datePub) {
        $this->datePub = $datePub;
    }

    function setTitre($titre) {
        $this->titre = $titre;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setPresentation($presentation) {
        $this->presentation = $presentation;
    }

    function setUtilisateur(Utilisateur $utilisateur) {
        $this->utilisateur = $utilisateur;
    }
    /**
    * Set livre
    *
    * @param SmartBook\LecteurBundle\Entity\Livre
    */
    function setLivre(Livre $livre) {
        
        $this->livre = $livre;
    }

        /**
     * @var \Livre
     *
     * @ORM\ManyToOne(targetEntity="Livre",cascade={"persist","remove"})
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="livre", referencedColumnName="id_l")
     * })
     */
    private $livre;


}
