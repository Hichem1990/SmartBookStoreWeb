<?php

namespace SmartBook\LecteurBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Librairie
 *
 * @ORM\Table(name="librairie", indexes={@ORM\Index(name="libraire", columns={"libraire"})})
 * @ORM\Entity
 */
class Librairie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_lib", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLib;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=100, nullable=false)
     */
    private $adresse;

    /**
     * @var float
     *
     * @ORM\Column(name="logtitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $logtitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $latitude;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="libraire", referencedColumnName="id")
     * })
     */
    private $libraire;

    function getIdLib() {
        return $this->idLib;
    }

    function getNom() {
        return $this->nom;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getLogtitude() {
        return $this->logtitude;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLibraire() {
        return $this->libraire;
    }

    function setIdLib($idLib) {
        $this->idLib = $idLib;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setLogtitude($logtitude) {
        $this->logtitude = $logtitude;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLibraire(\Utilisateur $libraire) {
        $this->libraire = $libraire;
    }


}
