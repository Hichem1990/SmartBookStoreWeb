<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PublicationRepository
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Entity;
use Doctrine\ORM\EntityRepository ;
class PublicationRepository extends EntityRepository {
    
    
    public function findByUtilisateur($id)
    {
        $query=$this->getEntityManager()->createQuery("SELECT p from SmartBookLecteurBundle:Publication p"
       . "JOIN ¨SmartBookLecteurBundle:Utilisateur u WHERE p.utilisateur.id=u.id AND u.id=:utilisateur")
        ->setParameter("utilisateur", $id);
        return $query->getResult();
    }
   
}
