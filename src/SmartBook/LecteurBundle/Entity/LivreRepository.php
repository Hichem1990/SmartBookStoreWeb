<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LivreRepository
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Entity;
use Doctrine\ORM\EntityRepository ;
class LivreRepository extends  EntityRepository {

  public function findByLibraire($id)
    {
      $query=$this->getEntityManager()->createQuery("SELECT l from SmartBookLecteurBundle:Livre l"
      ."JOIN ¨SmartBookLecteurBundle:Utilisateur u WHERE l.libraire.id=u.id AND u.id=:libraire")
       ->setParameter("libraire", $id);
       return $query->getResult();
       
    } 
    //put your code here
}
