<?php

namespace SmartBook\LecteurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    public function indexAction()
    {
        return $this->render('SmartBookLecteurBundle:Admin:index.html.twig', array());
    }
}
