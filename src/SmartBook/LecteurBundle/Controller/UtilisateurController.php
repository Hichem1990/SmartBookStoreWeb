<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UtilisateurController
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Controller;
use SmartBook\LecteurBundle\Entity\Utilisateur;
use SmartBook\LecteurBundle\Entity\Reclamation;
use SmartBook\LecteurBundle\Form\ReclamationType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class UtilisateurController extends Controller {
   
   public function ListAction()
    {
        $em=$this->getDoctrine()->getManager();
        $utilisateurs = $em->getRepository('SmartBookLecteurBundle:Utilisateur')->findAll();
        return $this->render("SmartBookLecteurBundle:Utilisateur:list.html.twig",array("utilisateurs"=>$utilisateurs));
}

    public function ListAdminAction()
    {
        $em=$this->getDoctrine()->getManager();
        $utilisateurs = $em->getRepository('SmartBookLecteurBundle:Utilisateur')->findAll();
        return $this->render("SmartBookLecteurBundle:Utilisateur:listAdmin.html.twig",array("utilisateurs"=>$utilisateurs));
     }
    public function deleteAction ($id)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $utilisateur = $em->getRepository('SmartBookLecteurBundle:Utilisateur')->find($id);
        $em->remove($utilisateur);
        $em->flush();
        return $this->redirect($this->generateUrl("smart_book"));
    }

    public function searchAction()
    {
       $request = $this->get('request_stack')->getCurrentRequest();
       if ($request->getMethod() == 'POST') { $em = $this->getDoctrine()->getManager(); $id = $request->get('id'); 
       $utilisateur = $em->getRepository("SmartBookLecteurBundle:Utilisateur")->findBy(array('id' => $id)); 
       return $this->render("SmartBookLecteurBundle:Utilisateur:resultat.html.twig", array('utilisateur' => $utilisateur)); 
  
      } 
       return $this->render("SmartBookLecteurBundle:Utilisateur:search.html.twig");
      }
   
       public function rechercheAction()
       {
       $request = $this->get('request_stack')->getCurrentRequest();
       if ($request->getMethod() == 'POST') { $em = $this->getDoctrine()->getManager(); $nom = $request->get('nom'); 
       $utilisateur = $em->getRepository("SmartBookLecteurBundle:Utilisateur")->findBy(array('nom' => $nom)); 
        return $this->render("SmartBookLecteurBundle:Utilisateur:resultat.html.twig", array('utilisateur' => $utilisateur)); 
  
        } 
        return $this->render("SmartBookLecteurBundle:Utilisateur:recherche.html.twig");
         }
         
  
        public function ajouterreclamationAction($id) { {
         $utilisateur = new Utilisateur;
         $reclamation = new Reclamation;
         $em = $this->getDoctrine()
          ->getManager();
         $utilisateur = $em->getRepository('SmartBookLecteurBundle:Utilisateur')
                    ->find($id);
            $reclamation->setUtilisateur($utilisateur);

            // J'ai raccourci cette partie, car c'est plus rapide à écrire !
            $form = $this->createForm(new ReclamationType, $reclamation);
            // On récupère la requête
            $request = $this->get('request');





            // On vérifie qu'elle est de type POST
            if ($request->getMethod() == 'POST') {

                $form->bind($request);

                if ($form->isValid()) {
                    // On l'enregistre notre objet $client dans la base de données
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($reclamation);
                    $em->flush();

                    // On redirige vers la page de visualisation de manager nouvellement créé
                    return $this->redirect($this->generateUrl('smart_book_reclam', array('id' => $utilisateur->getId())));
                }
            }

            // À ce stade :
            // - Soit la requête est de type GET, donc le visiteur vient d'arriver sur la page et veut voir le formulaire
            // - Soit la requête est de type POST, mais le formulaire n'est pas valide, donc on l'affiche de nouveau

            return $this->render('SmartBookLecteurBundle:Utilisateur:ajouterreclamation.html.twig', array(
                        'form' => $form->createView(),
                    ));
        }
    }
    
    
     public function voirreclamationAction($id) {

        // On récupère le repository
        $em = $this->getDoctrine()
                ->getManager();

        $manager = $em->getRepository('SmartBookLecteurBundle:Utilisateur')
                ->find($id);




        // $manager est donc une instance de Esprit\AdminBundle\Entity\Manager
        // Ou null si aucun manager n'a été trouvé avec l'id $id
        $liste_reclamation = $em->getRepository('SmartBookLecteurBundle:Reclamation')
                ->findAll();
        return $this->render('SmartBookLecteurBundle:Utilisateur:voirreclamation.html.twig', array(
                    'reclamations' => $liste_reclamation, 'utilisateur' => $utilisateur
                ));
    }
    
    public function AfficheLibrairesAction()
    {
     $em=$this->getDoctrine()->getManager();
     $Libraires = $em->getRepository('SmartBookLecteurBundle:Utilisateur')->findAll();
     return $this->render("SmartBookLecteurBundle:utilisateur:liste_libraires.html.twig",array("libraires"=> $Libraires));
    }
    public function comptelibraireAction()
    {
         return $this->render("SmartBookLecteurBundle:Livre:compte_libraire.html.twig");  
    }
   
    
}
