<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PublicationController
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use SmartBook\LecteurBundle\Entity\Publication ;
use SmartBook\LecteurBundle\Entity\Livre ;
use SmartBook\LecteurBundle\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class PublicationController extends Controller{
    
    public $id_u ;
    public function AfficherpublicationsAction()
    {
        $em=$this->getDoctrine()->getManager();
        $publications = $em->getRepository('SmartBookLecteurBundle:Publication')->findAll();
        return $this->render("SmartBookLecteurBundle:publication:liste_publications.html.twig",array("publications"=>$publications));
    }
    public function AfficherpublicationsByUtilisateurAction($id)
    {
        $this->id_u=$id ;
        $em=$this->getDoctrine()->getManager();
        $publications = $em->getRepository('SmartBookLecteurBundle:Publication')->findByUtilisateur($id);
        return $this->render("SmartBookLecteurBundle:publication:gerer_publications.html.twig",array("publications"=>$publications));
    }
    public function AfficherDetailAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $pub = $em->getRepository('SmartBookLecteurBundle:Publication')->find($id);
        return $this->render("SmartBookLecteurBundle:publication:detail_publication.html.twig",array("pub"=>$pub));
    }
    public function DeposerAnnonceAction($id,$type)
    {  
     $publication=new Publication();   
     $utilisateur=new Utilisateur();
     $livre=new Livre();  
     if($type=="achat")
     {
     $em= $this->getDoctrine()->getManager();
     $request = $this->get('request_stack')
                ->getCurrentRequest(); 
     if ($request->getMethod()=="POST")
        {
      $titre = $request->get('titre');
            $publication->setTitre($titre);
            $date_actuelle = new \DateTime();
            $date=$date_actuelle->format('Y-m-d');         
            $publication->setDatePub($date);
            $presentation = $request->get('presentation');
            $publication->setPresentation($presentation);
            $publication->setType($type);
            $utilisateur=$em->getRepository('SmartBookLecteurBundle:Utilisateur')->find($id);
            $publication->setUtilisateur($utilisateur);           
            $em ->persist($publication);
            $em ->flush();
            return $this->redirectToRoute('smart_book_lecteur_GererPub', array('id' => $id));
     }}
     else
     {
     $em= $this->getDoctrine()->getManager();
     $request = $this->get('request_stack')
                ->getCurrentRequest();
        if ($request->getMethod()=="POST")
        {
            $titre = $request->get('titre');
            $publication->setTitre($titre);
            $date_actuelle = new \DateTime();
            $date=$date_actuelle->format('Y-m-d');         
            $publication->setDatePub($date);
            $presentation = $request->get('presentation');
            $publication->setPresentation($presentation);
            $publication->setType($type);
            $utilisateur=$em->getRepository('SmartBookLecteurBundle:Utilisateur')->find($id);
            $publication->setUtilisateur($utilisateur); 
            $noml=$request->get('noml');
            $livre->setNomLivre($noml);
            $auteur=$request->get('auteur');
            $livre->setAuteur($auteur);
            $langue=$request->get('langue');
            $datee=$request->get('datee');          
            $livre->setDateEdition($datee);          
            $livre->setLangue($langue);
            $prix=$request->get('prix');
            $livre->setPrix($prix);
            $nbrp=$request->get('nbrp');
            $livre->setNbrPages($nbrp);
            $etat=$request->get('etat');
            $livre->setEtat($etat);
            $cat=$request->get('categorie');
            $livre->setCategorie($cat);
            $image=$request->get('image');
            $livre->setImageL($image);      
            $publication->setLivre($livre);            
            $em = $this->getDoctrine()->getManager();
            $livre->upload();
            $em ->persist($publication);
            $em ->flush();
            return $this->redirectToRoute('smart_book_lecteur_GererPub', array('id' => $id));
            
     }  }           
       return $this->render("SmartBookLecteurBundle:publication:deposer_annonce.html.twig",array("id"=>$id,"type"=>$type));
    }

    public function modifierAction($id,$type,$userid)
    {
     $this->id_u=$id;
     $publication=new Publication(); 
     $em= $this->getDoctrine()->getManager();
     $publication=$em->getRepository('SmartBookLecteurBundle:Publication')->find($id);
     $utilisateur=new Utilisateur();
     $livre=new Livre();  
     if($type=="achat")
     {    
     $request = $this->get('request_stack')
                ->getCurrentRequest(); 
     if ($request->getMethod()=="POST")
        {
      $titre = $request->get('titre');
            $publication->setTitre($titre);           
            $presentation = $request->get('presentation');
            $publication->setPresentation($presentation);
            $publication->setType($type);
            $utilisateur=$em->getRepository('SmartBookLecteurBundle:Utilisateur')->find($id);
            $publication->setUtilisateur($utilisateur);           
            $em ->persist($publication);
            $em ->flush();         
            return $this->redirect($this->generateUrl('smart_book_lecteur_GererPub', array('id' => $this->id_u)));
     }}
     else
     {
     $em= $this->getDoctrine()->getManager();
     $request = $this->get('request_stack')
                ->getCurrentRequest();
        if ($request->getMethod()=="POST")
        {
            $titre = $request->get('titre');
            $publication->setTitre($titre);
            $date_actuelle = new \DateTime();
            $date=$date_actuelle->format('Y-m-d');         
            $publication->setDatePub($date);
            $presentation = $request->get('presentation');
            $publication->setPresentation($presentation);
            $publication->setType($type);     
            $noml=$request->get('noml');
            $livre->setNomLivre($noml);
            $auteur=$request->get('auteur');
            $livre->setAuteur($auteur);
            $langue=$request->get('langue');
            $datee=$request->get('datee');
            if($datee!=null)
            {
            $livre->setDateEdition($datee);
            }
            $livre->setLangue($langue);
            $prix=$request->get('prix');
            $livre->setPrix($prix);
            $nbrp=$request->get('nbrp');
            $livre->setNbrPages($nbrp);
            $etat=$request->get('etat');
            $livre->setEtat($etat);
            $cat=$request->get('categorie');
            $livre->setCategorie($cat);
            $image=$request->get('image');
            if($image!=null)
            {
            $livre->setImageL($image);
            }
            $publication->setLivre($livre);            
            $em = $this->getDoctrine()->getManager();
            $em ->persist($publication);
            $em ->flush();
             return $this->redirectToRoute('smart_book_lecteur_GererPub', array('id' => $userid));
     }  }           
       return $this->render("SmartBookLecteurBundle:publication:modifier_annonce.html.twig",array("id"=>$id,"type"=>$type,"pub"=>$publication));
    }
    public function supprimerAction($id,$userid) {       
        $em = $this->getDoctrine()->getManager();
        $publication = $em->getRepository("SmartBookLecteurBundle:Publication")->find($id);
        $em->remove($publication);
        $em->flush();
          return $this->redirectToRoute('smart_book_lecteur_GererPub', array('id' => $userid));
                  //($this->generateUrl('smart_book_lecteur_GererPub',array('id' => $this->id_u) ));
    }
}
