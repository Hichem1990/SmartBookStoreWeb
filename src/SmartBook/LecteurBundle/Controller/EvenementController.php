<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EvenementController
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Controller;
use Symfony\Component\BrowserKit\Response ;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use SmartBook\LecteurBundle\Entity\Evenement;
use SmartBook\LecteurBundle\Entity\Utilisateur;
class EvenementController extends Controller {
   public function afficherAction()
    {
        
        $em = $this->getDoctrine() ->getManager();
        $evenemen = $em->getRepository('SmartBookLecteurBundle:Evenement')->findAll();
        return $this->render('SmartBookLecteurBundle:Evenement:liste_evenements.html.twig',
        
        array (
            'evenements'=>$evenemen
   ));
    }
     public function gestionevenAction($id)
    {
        
        $em = $this->getDoctrine() ->getManager();
        $evenemen = $em->getRepository('SmartBookLecteurBundle:Evenement')->findByUtilisateur($id);
        return $this->render('SmartBookLecteurBundle:Evenement:gerer_evenements.html.twig',
        
        array (
            'evenements'=>$evenemen
   ));
    }
    
    
     public function deleteAction ($id_e,$userid){
        $em = $this->getDoctrine()->getManager();
        $evenemen = $em -> getRepository('SmartBookLecteurBundle:Evenement')->find($id_e);
        $em->remove ($evenemen);
        $em->flush();
         return $this->redirectToRoute('smart_book_lecteur_GererEven', array('id' => $userid));
    }
    
    public function addAction ($id){
          $request=$this->get('request');
          $evenemen=new Evenement();
          $utilisateur =new Utilisateur();
          $em = $this->getDoctrine()->getManager();
          
        if($request->getMethod()=='POST')
            {
            $utilisateur=$em->getRepository('SmartBookLecteurBundle:Utilisateur')->find($id);
            $evenemen->setDateEvent($request->get("date_evenement"));
            $evenemen->setTitre($request->get("titre"));
            $evenemen->setUtilisateur($utilisateur);
            $evenemen->setDureeEve($request->get("Duree"));
            $evenemen->setLieuEve($request->get("lieu"));
            $evenemen->setDescriptionEve($request->get("description"));
            $evenemen->setImage($request->get("image"));                    
            $em->persist($evenemen);
            $em->flush();
        
            
        }
        return ($this->render("SmartBookLecteurBundle:Evenement:add.html.twig",array('e'=>$evenemen)));
}
    public function updateAction($id_e,$userid){
          
           $em=$this->getDoctrine()->getManager();
           $evenemen = $em->getRepository("SmartBookLecteurBundle:Evenement")->find($id_e);
           $request=$this->get('request');         
            if($request->getMethod()=='POST')
            {
            $evenemen->setDateEvent($request->get("date_evenement"));
            $evenemen->setTitre($request->get("titre"));           
            $evenemen->setDureeEve($request->get("Duree"));
            $evenemen->setLieuEve($request->get("lieu"));
            $evenemen->setDescriptionEve($request->get("description"));
            $evenemen->setImage($request->get("image"));       
            $em->persist($evenemen);
            $em->flush(); 
            return $this->redirectToRoute('smart_book_lecteur_GererEven', array('id' =>$userid));
           }
           return $this->render('SmartBookLecteurBundle:Evenement:update.html.twig',array('e'=>$evenemen));
        
    }
       public function ParticiperAction($id_e)
       {
          $em=$this->getDoctrine()->getManager();
          $evenement = $em->getRepository("SmartBookLecteurBundle:Evenement")->find($id_e);
          $evenement-> setNbrParticipant($evenement->getNbrParticipant()+1);
          $em->persist($evenement);
          $em->flush();
          return $this->redirectToRoute('smart_book_lecteur_affiche');
       }
    
     public function rechercheAction() {
        $request = $this->get('request_stack')->getCurrentRequest();
        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $titre = $request->get('titre');
            $evenemen = $em->getRepository("SmartBookLecteurBundle:Evenement")->findBy(array('titre' => $titre));
            return $this->render("SmartBookLecteurBundle:Evenement:resultat.html.twig", array('evenemen' => $evenemen));
        }
        return $this->render("SmartBookLecteurBundle:Evenement:recherche.html.twig");
    }
    
    
    public function affichePdfAction($id)
      
    {
        $em=$this->getDoctrine()->getManager();
         $evenemen = $em->getRepository("SmartBookLecteurBundle:Evenement")->find($id);
        $html=$this->renderView('SmartBookLecteurBundle:Evenement:affichepdf.html.twig',
        array("evenement"=>$evenemen));
        return new Response(
        $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
             200,
             array(
                'content-type'=>'application/pdf',
                  'content-Disposition'=>'attachment;filename="file.pdf"'
              )
                
                );
           
    }
    
} 



    
    

 