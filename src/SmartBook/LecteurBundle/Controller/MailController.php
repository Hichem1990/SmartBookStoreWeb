<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MailController
 *
 * @author khayri
 */
namespace SmartBook\LecteurBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SmartBook\LecteurBundle\Entity\Mail;
use SmartBook\LecteurBundle\Form\MailType;
class MailController extends Controller {

    public function indexAction() {
        return $this->render('SmartBookLecteurBundle:Publication:mail.html.twig', array());

        return 'Mail';
    }

   

    public function newAction($touser) {
        $to =$touser;
        $mail = new Mail();
        $form = $this->createForm(new MailType(), $mail);
        //$request->get('request');
        $request = $this->get('request'); 
//        $request = $this->get('request_stack')
//                ->getCurrentRequest();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $message = \Swift_Message::newInstance()
                    ->setSubject($mail->getNom())
                    ->setFrom($mail->getFrom())
                    ->setTo($to)
                    ->setBody($mail->getText());
            $this->get('mailer')->send($message);
            return $this->render('SmartBookLecteurBundle:Publication:mail.html.twig',array());
            
            
        }
        return $this->render('SmartBookLecteurBundle:Publication:new.html.twig', array('to' => $to,
            'from' => $mail->getFrom(),'form'=>$form->createView()
                ));
    }

}
