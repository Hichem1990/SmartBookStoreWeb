<?php

namespace SmartBook\LecteurBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;
use SmartBook\LecteurBundle\Controller\LivreController;
use Zend\Json\Expr;
class GrapheController extends Controller{
public function chartPieAction(){
$ob = new Highchart();
$ob->chart->renderTo('piechart');
$ob->title->text('La liste des Livres selon chaque categorie qui sont disponibles');
$ob->plotOptions->pie(array(
'allowPointSelect' => true,
'cursor' => 'pointer',
'dataLabels' => array('enabled' => false),
'showInLegend' => true
));
$em= $this->getDoctrine()->getManager();
        $query = $em->createQuery('select livre FROM SmartBookLecteurBundle:Livre livre');
        $livre = $query->getResult();
        $data=  [];
       for ($i = 0; $i < Count($livre); $i++) {
           
           $data[$i]=array($livre[$i]->getCategorie(),$i+1,);
       }
//$data = array(
//array('Technologie', 45.0),
//array('Langues et Linguistique', 26.8),
//array('Informatique & Internet', 12.8),
//array('Cuisine', 8.5),
//array('Poésie', 6.2),
//array('Arts', 0.7),
//array('others', 5.1),
//);
$ob->series(array(array('type' => 'pie','name' => 'Browser share', 'data' => $data)));
return $this->render('SmartBookLecteurBundle:Graphe:pie.html.twig', array(
'chart' => $ob
));
}
}
    
/*public function chartHistogrammeAction()
{
$series = array(
array(
'name' => 'categorie',
'type' => 'column',
'color' => '#4572A7',
'yAxis' => 1,
'data' => array(49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4),
),
);
$yData = array(
array(
'labels' => array(
//'formatter' => new Expr('function () { return this.value + " degrees C" }'),
'style' => array('color' => '#AA4643')
),

'opposite' => true,
),
array(
'labels' => array(
//'formatter' => new Expr('function () { return this.value + " mm" }'),
'style' => array('color' => '#4572A7')
),
'gridLineWidth' => 0,
'title' => array(
'text' => 'categorie',
'style' => array('color' => '#4572A7')
),
),
);
       
$mois = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
$ob = new Highchart();
$ob->chart->renderTo('container'); // The #id of the div where to render the chart
$ob->chart->type('column');
$ob->title->text('Liste des Livres par catégorie');
$ob->xAxis->categories($mois);
$ob->yAxis($yData);
$ob->legend->enabled(false);
$formatter = new Expr('function () {
var unit = {
"Rainfall": "mm",
"Temperature": "degrees C"
}[this.series.name];
return this.x + ": <b>" + this.y + "</b> " + unit;
}');
$ob->tooltip->formatter($formatter);
$ob->series($series);
return $this->render('SmartBookGrapheBundle:Graphe:histogramme.html.twig', array(
'chart' => $ob
));
}
}
//   public function chartLineAction(){
//$em=$this->getDoctrine()->getEntityManager();
//$series = array(
//array("name" => "Nom du graphe", "data" => array(3,5,1,7,2,2,8))
//);
//$ob = new Highchart();
//$ob->chart->renderTo('linechart'); // #id du div où afficher le graphe
//$ob->title->text('Liste des Livres par catégorie');
//$ob->xAxis->title(array('text' => "Mois"));
//$ob->yAxis->title(array('text' => "Categorie "));
//$ob->series($series);
//return $this->render('SmartBookGrapheBundle:Graphe:LineChart.html.twig', array(
//'chart' => $ob
//));
//   }
//}
        

