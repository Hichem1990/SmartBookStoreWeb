<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LibrairieController
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Controller;
use SmartBook\LecteurBundle\Entity\Librairie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class LibrairieController extends Controller{
    
     public function localiserAction()
    {
      $em=$this->getDoctrine()->getManager();
      $librairies = $em->getRepository('SmartBookLecteurBundle:Librairie')->findAll();
      return $this->render("SmartBookLecteurBundle:librairie:localisation.html.twig",array('librairies'=>$librairies));  
    }
     public function FindLibrairieAction()
    {
      $librairie=new Librairie();
      $request = $this->get('request_stack')
         ->getCurrentRequest();
       $em=$this->getDoctrine()->getManager();
     if ($request->getMethod()=="POST")
        {
          $id = $request->get('lib');            
          $librairie=$em->getRepository('SmartBookLecteurBundle:Librairie')->find($id);     
           
        }
       return $this->render("SmartBookLecteurBundle:librairie:affiche_librairie.html.twig",array('librairie'=>$librairie));
     
    }
}
