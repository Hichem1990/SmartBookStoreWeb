<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LivreController
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SmartBook\LecteurBundle\Entity\Livre ;
class LivreController  extends Controller{
     
    
    public function addAction(){
        $request = $this->get('request');
         $livre = new Livre();
         if ($request->getMethod()=='POST')
         {
            
            $livre->setNomLivre($request->get('noml'));       
            $livre->setImageL($request->get('image'));
            $livre->setAuteur($request->get('auteur'));
            $livre->setLangue($request->get('langue'));
            $livre->setPrix($request->get('prix'));
            $livre->setNbrPages($request->get('nbrp'));
            $livre->setDateEdition($request->get('date'));
            $livre->setEtat($request->get('etat'));
            $livre->setCategorie($request->get('categorie'));                             
            $em = $this->getDoctrine()->getManager();
            $em ->persist($livre);
            $em ->flush();
        }      
        return $this->render('SmartBookLecteurBundle:Livre:ajout.html.twig', array());
    }
            
        public function listAction(){ 
            $em=$this->getDoctrine()->getManager();
            $livre=$em->getRepository('SmartBookLecteurBundle:Livre')->findAll();
            return $this->render('SmartBookLecteurBundle:Livre:list.html.twig', array('livres'=>$livre));
        }
              
        public function listlivresAction($id){ 
            $em=$this->getDoctrine()->getManager();
            $livre=$em->getRepository('SmartBookLecteurBundle:Livre')->findByLibraire($id);
            return $this->render('SmartBookLecteurBundle:Livre:list_livres.html.twig', array('livres'=>$livre));
        }
        
        public function deleteAction($idL){ 
            $em= $this->getDoctrine()->getManager();
            $livre=$em->getRepository("SmartBookLecteurBundle:Livre")->find($idL);
            $em->remove($livre);
            $em->flush();
            return ($this->redirectToRoute("smart_book_livre_affiche"));
        }
        
            
        
        public function updateAction($idL){
           $l=new Livre();
           $em=$this->getDoctrine()->getManager();
           $livre = $em->getRepository("SmartBookLecteurBundle:Livre")->find($idL);
           $request=$this->get('request');         
            if($request->getMethod()=='POST')
            {
                
            $livre->setNomLivre($request->get('noml'));       
            $livre->setImageL($request->get('image'));
            $livre->setAuteur($request->get('auteur'));
            $livre->setLangue($request->get('langue'));
            $livre->setPrix($request->get('prix'));
            $livre->setNbrPages($request->get('nbrp'));
            $livre->setDateEdition($request->get('date'));
            $livre->setEtat($request->get('etat'));
            $livre->setCategorie($request->get('categorie'));                             
            $em = $this->getDoctrine()->getManager();
            $em ->flush();
            return $this ->redirectToRoute("smart_book_livre_affiche");
           }
           return $this->render('SmartBookLecteurBundle:Livre:update.html.twig',array('l'=>$livre));
        
    }
    
      public function rechercheAction() {
       $livres=new Livre();
       $request = $this->get('request_stack')->getCurrentRequest();
        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $nomLivre = $request->get('nomLivre');
            $etat = $request->get('etat');
            $livres = $em->getRepository("SmartBookLecteurBundle:Livre")->findBy(array('nomLivre'=> $nomLivre,'etat'=> $etat));
            return $this->render("SmartBookLecteurBundle:Livre:resultat.html.twig", array('livres' => $livres));
        }
        return $this->render("SmartBookLecteurBundle:Livre:rechercher.html.twig");
    }
}
