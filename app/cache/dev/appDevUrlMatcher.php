<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/Lecteur')) {
            if (0 === strpos($pathinfo, '/Lecteur/A')) {
                // smart_book_lecteur_homepage
                if ($pathinfo === '/Lecteur/Accueil') {
                    return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\LecteurController::indexAction',  '_route' => 'smart_book_lecteur_homepage',);
                }

                // smart_book_lecteur_Authentification
                if ($pathinfo === '/Lecteur/Authentification') {
                    return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::authentificationAction',  '_route' => 'smart_book_lecteur_Authentification',);
                }

            }

            // smart_book_lecteur_Inscription
            if ($pathinfo === '/Lecteur/Inscription') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::inscriptionAction',  '_route' => 'smart_book_lecteur_Inscription',);
            }

            // smart_book_lecteur_Modification
            if ($pathinfo === '/Lecteur/Modification') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::modificationAction',  '_route' => 'smart_book_lecteur_Modification',);
            }

            // smart_book_lecteur_Desactivation
            if ($pathinfo === '/Lecteur/Desactivation') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::desactivationAction',  '_route' => 'smart_book_lecteur_Desactivation',);
            }

            // smart_book_lecteur_Activation
            if ($pathinfo === '/Lecteur/Activation') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::activationAction',  '_route' => 'smart_book_lecteur_Activation',);
            }

            // smart_book_lecteur_Deposer
            if (0 === strpos($pathinfo, '/Lecteur/Deposer') && preg_match('#^/Lecteur/Deposer/(?P<id>[^/]++)/(?P<type>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_Deposer')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\PublicationController::deposerannonceAction',));
            }

            // smart_book_lecteur_Affichage
            if ($pathinfo === '/Lecteur/Affichage') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\PublicationController::afficherpublicationsAction',  '_route' => 'smart_book_lecteur_Affichage',);
            }

            // smart_book_lecteur_GererPub
            if (0 === strpos($pathinfo, '/Lecteur/GererPub') && preg_match('#^/Lecteur/GererPub/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_GererPub')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\PublicationController::afficherpublicationsbyutilisateurAction',));
            }

            // smart_book_lecteur_Sup_Pub
            if (0 === strpos($pathinfo, '/Lecteur/SupPub') && preg_match('#^/Lecteur/SupPub/(?P<id>[^/]++)/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_Sup_Pub')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\PublicationController::supprimerAction',));
            }

            // smart_book_lecteur_ModifPub
            if (0 === strpos($pathinfo, '/Lecteur/ModifPub') && preg_match('#^/Lecteur/ModifPub/(?P<id>[^/]++)/(?P<type>[^/]++)/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_ModifPub')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\PublicationController::modifierAction',));
            }

            // smart_book_lecteur_DetailPub
            if (0 === strpos($pathinfo, '/Lecteur/Detail_Pub') && preg_match('#^/Lecteur/Detail_Pub/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_DetailPub')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\PublicationController::afficherdetailAction',));
            }

            // smart_book_lecteur_Localisation
            if ($pathinfo === '/Lecteur/Localiser') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\LibrairieController::localiserAction',  '_route' => 'smart_book_lecteur_Localisation',);
            }

            if (0 === strpos($pathinfo, '/Lecteur/A')) {
                if (0 === strpos($pathinfo, '/Lecteur/Affiche_librair')) {
                    // smart_book_lecteur_AfficherLibrairie
                    if ($pathinfo === '/Lecteur/Affiche_librairie') {
                        return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\LibrairieController::findLibrairieAction',  '_route' => 'smart_book_lecteur_AfficherLibrairie',);
                    }

                    // smart_book_lecteur_AfficherLibraire
                    if ($pathinfo === '/Lecteur/Affiche_libraire') {
                        return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::affichelibrairesAction',  '_route' => 'smart_book_lecteur_AfficherLibraire',);
                    }

                }

                // smart_book_livre_Ajout
                if ($pathinfo === '/Lecteur/AjoutLivre') {
                    return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\LivreController::addAction',  '_route' => 'smart_book_livre_Ajout',);
                }

                // smart_book_livre_affiche
                if ($pathinfo === '/Lecteur/AfficheLivre') {
                    return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\LivreController::listAction',  '_route' => 'smart_book_livre_affiche',);
                }

            }

            // smart_book_livre_Delete
            if (0 === strpos($pathinfo, '/Lecteur/SupprimerLivre') && preg_match('#^/Lecteur/SupprimerLivre/(?P<idL>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_livre_Delete')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\LivreController::deleteAction',));
            }

            // Smart_book_livre_Update
            if (0 === strpos($pathinfo, '/Lecteur/ModifierLivre') && preg_match('#^/Lecteur/ModifierLivre/(?P<idL>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Smart_book_livre_Update')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\LivreController::updateAction',));
            }

            // Smart_book_livre_Rechercher
            if ($pathinfo === '/Lecteur/RechercherLivre') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\LivreController::rechercheAction',  '_route' => 'Smart_book_livre_Rechercher',);
            }

            if (0 === strpos($pathinfo, '/Lecteur/Affiche')) {
                // smart_book_livre_afficheLibraire
                if (0 === strpos($pathinfo, '/Lecteur/AfficheLivreLibraire') && preg_match('#^/Lecteur/AfficheLivreLibraire/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_livre_afficheLibraire')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\LivreController::listLivresAction',));
                }

                // smart_book_lecteur_affiche
                if ($pathinfo === '/Lecteur/Affiche') {
                    return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\EvenementController::afficherAction',  '_route' => 'smart_book_lecteur_affiche',);
                }

            }

            // smart_book_lecteur_GererEven
            if (0 === strpos($pathinfo, '/Lecteur/GererEven') && preg_match('#^/Lecteur/GererEven/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_GererEven')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\EvenementController::gestionevenAction',));
            }

            // smart_book_lecteur_delete
            if (0 === strpos($pathinfo, '/Lecteur/delModele') && preg_match('#^/Lecteur/delModele/(?P<id_e>[^/]++)/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_delete')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\EvenementController::deleteAction',));
            }

            // smart_book_lecteur_add
            if (0 === strpos($pathinfo, '/Lecteur/addEvenement') && preg_match('#^/Lecteur/addEvenement/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_add')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\EvenementController::addAction',));
            }

            // smart_book_lecteur_update
            if (0 === strpos($pathinfo, '/Lecteur/updateEvenement') && preg_match('#^/Lecteur/updateEvenement/(?P<id_e>[^/]++)/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_update')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\EvenementController::updateAction',));
            }

            // smart_book_lecteur_participer
            if (0 === strpos($pathinfo, '/Lecteur/participer') && preg_match('#^/Lecteur/participer/(?P<id_e>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_participer')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\EvenementController::participerAction',));
            }

            // smart_book_lecteur_recherche
            if ($pathinfo === '/Lecteur/rechercheEvenement') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\EvenementController::rechercheAction',  '_route' => 'smart_book_lecteur_recherche',);
            }

            // smart_book_lecteur_Email
            if (0 === strpos($pathinfo, '/Lecteur/Envoie') && preg_match('#^/Lecteur/Envoie/(?P<touser>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_Email')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\MailController::newAction',));
            }

            // espritadmin_accueil
            if ($pathinfo === '/Lecteur/admin') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\AdminController::indexAction',  '_route' => 'espritadmin_accueil',);
            }

            // smart_book_lecteur_Librairie
            if ($pathinfo === '/Lecteur/Accueil_Libraire') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::comptelibraireAction',  '_route' => 'smart_book_lecteur_Librairie',);
            }

            // smart_book_lecteur_pie
            if ($pathinfo === '/Lecteur/pie') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\GrapheController::chartPieAction',  '_route' => 'smart_book_lecteur_pie',);
            }

            // smart_book
            if ($pathinfo === '/Lecteur/listUtilisateur') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::listAction',  '_route' => 'smart_book',);
            }

            // smart_book_reclam
            if ($pathinfo === '/Lecteur/ReclamUtilisateur') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::listAdminAction',  '_route' => 'smart_book_reclam',);
            }

            // smart_book_delete_utilisateur
            if (0 === strpos($pathinfo, '/Lecteur/deleteUtilisateur') && preg_match('#^/Lecteur/deleteUtilisateur/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_delete_utilisateur')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::deleteAction',));
            }

            // mart_book_lecteur_recherche
            if ($pathinfo === '/Lecteur/rechercheUtilisateur') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::rechercheAction',  '_route' => 'mart_book_lecteur_recherche',);
            }

            // Update_Utilisateur
            if ($pathinfo === '/Lecteur/updateUtilisateur') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::updateAction',  '_route' => 'Update_Utilisateur',);
            }

            // mart_book_lecteur_search
            if ($pathinfo === '/Lecteur/recherche') {
                return array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::searchAction',  '_route' => 'mart_book_lecteur_search',);
            }

            // espritadmin_ajouterreclamation
            if (0 === strpos($pathinfo, '/Lecteur/admin/ajouterreclamation') && preg_match('#^/Lecteur/admin/ajouterreclamation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'espritadmin_ajouterreclamation')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::ajouterreclamationAction',));
            }

            // smart_book_reclame_utilisateur
            if (0 === strpos($pathinfo, '/Lecteur/reclameUtilisateur') && preg_match('#^/Lecteur/reclameUtilisateur/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_reclame_utilisateur')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\UtilisateurController::ajouterreclamationAction',));
            }

            // smart_book_lecteur_pdf
            if (0 === strpos($pathinfo, '/Lecteur/AffichePdf') && preg_match('#^/Lecteur/AffichePdf/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'smart_book_lecteur_pdf')), array (  '_controller' => 'SmartBook\\LecteurBundle\\Controller\\EvenementController::affichePdfAction',));
            }

        }

        // homepage
        if ($pathinfo === '/app/example') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }

            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        if (0 === strpos($pathinfo, '/demo')) {
            if (0 === strpos($pathinfo, '/demo/secured')) {
                if (0 === strpos($pathinfo, '/demo/secured/log')) {
                    if (0 === strpos($pathinfo, '/demo/secured/login')) {
                        // _demo_login
                        if ($pathinfo === '/demo/secured/login') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
                        }

                        // _demo_security_check
                        if ($pathinfo === '/demo/secured/login_check') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_demo_security_check',);
                        }

                    }

                    // _demo_logout
                    if ($pathinfo === '/demo/secured/logout') {
                        return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/demo/secured/hello')) {
                    // acme_demo_secured_hello
                    if ($pathinfo === '/demo/secured/hello') {
                        return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
                    }

                    // _demo_secured_hello
                    if (preg_match('#^/demo/secured/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',));
                    }

                    // _demo_secured_hello_admin
                    if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello_admin')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',));
                    }

                }

            }

            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }

                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
