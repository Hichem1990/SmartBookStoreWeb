<?php

/* SmartBookLecteurBundle:Livre:update.html.twig */
class __TwigTemplate_3ed46578bc46893e7cb75d7d916506e91cd0a73a6e17d9c2547898609580adda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 6
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::compte_libraire.html.twig", "SmartBookLecteurBundle:Livre:update.html.twig", 6);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::compte_libraire.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_contenu($context, array $blocks = array())
    {
        // line 9
        echo "    <center>
    <h1>Mise à jour </h1>
    </center>
    <center>
    <form method=\"POST\"> 
   
        <table border=\"0\" cellpadding=\"15\">
   <tr>
       <td><label>Nom du Livre</label></td>
       <td><input name=\"noml\" type=\"text\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "getNomLivre", array(), "method"), "html", null, true);
        echo "\" required/></td>
   </tr>
   <tr>
      <td><label>Image du Livre</label></td>
       <td><input name=\"image\" type=\"file\" style=\"width:310px\" required/></td>
   </tr>
   <tr>
      <td><label>Nom d'auteur du Livre</label></td>
       <td><input name=\"auteur\" type=\"text\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "getAuteur", array(), "method"), "html", null, true);
        echo "\" required/></td>
   </tr>
   <tr>
      <td><label>Langue du Livre</label></td>
       <td><input name=\"langue\" type=\"text\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "getLangue", array(), "method"), "html", null, true);
        echo "\" required/></td>
   </tr>
   <tr>
      <td><label>Prix du Livre</label></td>
       <td><input  name=\"prix\" type=\"number\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "getPrix", array(), "method"), "html", null, true);
        echo "\" required/></td>
   </tr>
   <tr>
      <td><label>Nombre des pages du Livre</label></td>
       <td><input name=\"nbrp\" type=\"number\" value=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "getNbrPages", array(), "method"), "html", null, true);
        echo "\" required/></td>
   </tr>
   <tr>
      <td><label>Date Edition</label></td>
       <td><input name=\"date\" type=\"date\" style=\"width:310px\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "getDateEdition", array(), "method"), "html", null, true);
        echo "\" required/></td>
   </tr>
   <tr>
      <td><label>Etat du Livre</label></td>
       <td><input name=\"etat\" type=\"text\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "getEtat", array(), "method"), "html", null, true);
        echo "\" required/></td>
   </tr>
   <tr>
      <td><label>Categorie du Livre</label></td>
       <td><input name=\"categorie\" type=\"text\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "getCategorie", array(), "method"), "html", null, true);
        echo "\" required/></td>
   </tr>
        </table><br>
        <table>
   <tr>
       <td align=\"left\">
           <input type='submit' value=\"Modifier\" style=\"width:130px\"/>
           
       </td>
   </tr>
</table>
    </center>
        
    ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Livre:update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 50,  88 => 46,  81 => 42,  74 => 38,  67 => 34,  60 => 30,  53 => 26,  42 => 18,  31 => 9,  28 => 8,  11 => 6,);
    }
}
/* */
/* */
/* */
/* {# empty Twig template #}*/
/* {# Ressources/views/Livre/ajout.thml.twig #}*/
/* {% extends "SmartBookLecteurBundle::compte_libraire.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <center>*/
/*     <h1>Mise à jour </h1>*/
/*     </center>*/
/*     <center>*/
/*     <form method="POST"> */
/*    */
/*         <table border="0" cellpadding="15">*/
/*    <tr>*/
/*        <td><label>Nom du Livre</label></td>*/
/*        <td><input name="noml" type="text" value="{{l.getNomLivre()}}" required/></td>*/
/*    </tr>*/
/*    <tr>*/
/*       <td><label>Image du Livre</label></td>*/
/*        <td><input name="image" type="file" style="width:310px" required/></td>*/
/*    </tr>*/
/*    <tr>*/
/*       <td><label>Nom d'auteur du Livre</label></td>*/
/*        <td><input name="auteur" type="text" value="{{l.getAuteur()}}" required/></td>*/
/*    </tr>*/
/*    <tr>*/
/*       <td><label>Langue du Livre</label></td>*/
/*        <td><input name="langue" type="text" value="{{l.getLangue()}}" required/></td>*/
/*    </tr>*/
/*    <tr>*/
/*       <td><label>Prix du Livre</label></td>*/
/*        <td><input  name="prix" type="number" value="{{l.getPrix()}}" required/></td>*/
/*    </tr>*/
/*    <tr>*/
/*       <td><label>Nombre des pages du Livre</label></td>*/
/*        <td><input name="nbrp" type="number" value="{{l.getNbrPages()}}" required/></td>*/
/*    </tr>*/
/*    <tr>*/
/*       <td><label>Date Edition</label></td>*/
/*        <td><input name="date" type="date" style="width:310px" value="{{l.getDateEdition()}}" required/></td>*/
/*    </tr>*/
/*    <tr>*/
/*       <td><label>Etat du Livre</label></td>*/
/*        <td><input name="etat" type="text" value="{{l.getEtat()}}" required/></td>*/
/*    </tr>*/
/*    <tr>*/
/*       <td><label>Categorie du Livre</label></td>*/
/*        <td><input name="categorie" type="text" value="{{l.getCategorie()}}" required/></td>*/
/*    </tr>*/
/*         </table><br>*/
/*         <table>*/
/*    <tr>*/
/*        <td align="left">*/
/*            <input type='submit' value="Modifier" style="width:130px"/>*/
/*            */
/*        </td>*/
/*    </tr>*/
/* </table>*/
/*     </center>*/
/*         */
/*     {% endblock %}*/
/*     */
/* */
/* */
/* */
