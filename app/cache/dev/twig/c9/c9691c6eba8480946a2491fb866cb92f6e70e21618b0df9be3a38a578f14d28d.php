<?php

/* SmartBookLecteurBundle:Evenement:add.html.twig */
class __TwigTemplate_7e01c2e7ad077aa01c7c9e541049bf12f5e01990a001c7b33d75ee23ba751943 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:Evenement:add.html.twig", 3);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_contenu($context, array $blocks = array())
    {
        // line 6
        echo "    <center> 
    <form method=\"POST\" action=\"\"> 
        <table border=\"0\" cellpadding=\"15\">
   <tr>
       <td><label><b>date_evenement</b></label></td>
       <td><input type=\"date\" name=\"date_evenement\" required/></td>
   </tr>
   <tr>
       <td><label><b>titre</b></label></td>
       <td><input type=\"text\" name=\"titre\" required/></td>
   </tr>    
   <tr>
       <td><label><b>Duree</b></label></td>
       <td><input type=\"number\" name=\"Duree\" required/></td>
       <td><b>Minutes<b></td>
   </tr>
   <tr>
       <td><label><b>Lieu</b></label></td>
       <td><input type=\"text\" name=\"lieu\" required/></td>
   </tr>
   <tr>
       <td><label><b>Description</b></label></td>
       <td><textarea name=\"description\" required></textarea></td>
   </tr>
   <tr>
       <td><label><b>Image evenement</b></label></td>
       <td><input type=\"file\" name=\"image\" style=\"width:310px\" required/></td>
   </tr>
     
   <tr>
       <td align=\"left\">
           <input type='submit' value=\"Ajouter\" style=\"width:130px\"/>
           
       </td>
   </tr>
</table>
    </form>     
    ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Evenement:add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 6,  28 => 5,  11 => 3,);
    }
}
/* {# empty Twig template #}*/
/* */
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <center> */
/*     <form method="POST" action=""> */
/*         <table border="0" cellpadding="15">*/
/*    <tr>*/
/*        <td><label><b>date_evenement</b></label></td>*/
/*        <td><input type="date" name="date_evenement" required/></td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><b>titre</b></label></td>*/
/*        <td><input type="text" name="titre" required/></td>*/
/*    </tr>    */
/*    <tr>*/
/*        <td><label><b>Duree</b></label></td>*/
/*        <td><input type="number" name="Duree" required/></td>*/
/*        <td><b>Minutes<b></td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><b>Lieu</b></label></td>*/
/*        <td><input type="text" name="lieu" required/></td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><b>Description</b></label></td>*/
/*        <td><textarea name="description" required></textarea></td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><b>Image evenement</b></label></td>*/
/*        <td><input type="file" name="image" style="width:310px" required/></td>*/
/*    </tr>*/
/*      */
/*    <tr>*/
/*        <td align="left">*/
/*            <input type='submit' value="Ajouter" style="width:130px"/>*/
/*            */
/*        </td>*/
/*    </tr>*/
/* </table>*/
/*     </form>     */
/*     {% endblock %}*/
