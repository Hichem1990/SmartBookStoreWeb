<?php

/* FOSUserBundle::layout2.html.twig */
class __TwigTemplate_cfd157848cbd3a0a24fb613e481cdb3729552b49b7bb8b90fd0f39aa9d6b1e83 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html>
<head>
<title>Book Store</title>
<!--[if lt IE 9]>
\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
<![endif]-->
<!--[if lt IE 9]>
\t<script src=\"http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js\"></script>
<![endif]-->
<meta http-equiv=\"cache-control\" content=\"no-cache\"></meta>
<meta charset=\"utf-8\"></meta>
<meta name=\"viewport\" content=\"initial-scale=1, maximum-scale=1\"></meta>
<meta name=\"viewport\" content=\"width=device-width\"></meta>
<!-- Css Files Start -->
<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style_personnalisé.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- All css -->
<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bs.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Bootstrap Css -->
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main-slider.css"), "html", null, true);
        echo "\" /><!-- Main Slider Css -->
<!--[if lte IE 10]><link rel=\"stylesheet\" type=\"text/css\" href=\"css/customIE.css\" /><![endif]-->
<link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.css\""), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Font Awesome Css -->
<link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome-ie7.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Font Awesome iE7 Css -->
<noscript>
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/noJS.css"), "html", null, true);
        echo "\" />
</noscript>
<!-- Css Files End -->
 <script
    src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAMjKGV-a0gQYCeYJCrdDT2JThpg9U4ydk
    &callback=initMap\">
    </script>
    <script type=\"text/javascript\">
    var map;
    function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 8
   });
    }
    </script>
</head>
<body>
<!-- Start Main Wrapper -->
<div class=\"wrapper\"> 
  <!-- End Top Nav Bar -->
  <header id=\"main-header\">
    <section class=\"container-fluid container\">
      <section class=\"row-fluid\">
        <section class=\"span4\">
          <h1 id=\"logo\"> <a href=\"index.html\"><img src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" /></a> </h1>
        </section>
     
      </section>
    </section>
    <!-- Start Main Nav Bar -->
    <nav id=\"nav\">
      <div class=\"navbar navbar-inverse\">
        <div class=\"navbar-inner\">
          <button type=\"button\" class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\"> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> </button>
          
          <!--/.nav-collapse -->
        </div>
        <!-- /.navbar-inner -->
      </div>
      <!-- /.navbar -->
    </nav>
    <!-- End Main Nav Bar -->
  </header>
  ";
        // line 69
        $this->displayBlock('contenu', $context, $blocks);
        // line 72
        echo "   
 
   
  <!-- End Footer Top 2 -->
  <!-- Start Main Footer -->
  <footer id=\"main-footer\">
    <section class=\"social-ico-bar\">
      <section class=\"container\">
        <section class=\"row-fluid\">
          <article class=\"span6\">
            <p>© Le 352 Esprit 2015 </p>
          </article>
          <article class=\"span6 copy-right\">
            <p>Designed by <a href=\"\">Le352.com</a></p>
          </article>
        </section>
      </section>
    </section>
  </footer>
<!-- End Main Wrapper -->
<!-- JS Files Start -->
<script type=\"text/javascript\" src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/lib.js"), "html", null, true);
        echo "\"></script><!-- lib Js -->
<script type=\"text/javascript\" src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr.js"), "html", null, true);
        echo "\"></script><!-- Modernizr -->
<script type=\"text/javascript\" src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/easing.js"), "html", null, true);
        echo "\"></script><!-- Easing js -->
<script type=\"text/javascript\" src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bs.js"), "html", null, true);
        echo "\"></script><!-- Bootstrap -->
<script type=\"text/javascript\" src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bxslider.js"), "html", null, true);
        echo "\"></script><!-- BX Slider -->
<script type=\"text/javascript\" src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/input-clear.js"), "html", null, true);
        echo "\"></script><!-- Input Clear -->
<script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/range-slider.js"), "html", null, true);
        echo "\"></script><!-- Range Slider -->
<script src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.zoom.js"), "html", null, true);
        echo "\"></script><!-- Zoom Effect -->
<script type=\"text/javascript\" src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bookblock.js"), "html", null, true);
        echo "\"></script><!-- Flip Slider -->
<script type=\"text/javascript\" src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/custom.js"), "html", null, true);
        echo "\"></script><!-- Custom js -->
<script type=\"text/javascript\" src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/social.js"), "html", null, true);
        echo "\"></script><!-- Social Icons -->
<!-- JS Files End -->
<noscript>
<style>
\t#socialicons>a span { top: 0px; left: -100%; -webkit-transition: all 0.3s ease; -moz-transition: all 0.3s ease-in-out; -o-transition: all 0.3s ease-in-out; -ms-transition: all 0.3s ease-in-out; transition: all 0.3s \tease-in-out;}
\t#socialicons>ahover div{left: 0px;}
\t</style>
</noscript>
<script type=\"text/javascript\">
  /* <![CDATA[ */
  \$(document).ready(function() {
  \$('.social_active').hoverdir( {} );
})
/* ]]> */
</script>
</body>
</html>
";
    }

    // line 69
    public function block_contenu($context, array $blocks = array())
    {
        // line 70
        echo "
  ";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 70,  200 => 69,  178 => 103,  174 => 102,  170 => 101,  166 => 100,  162 => 99,  158 => 98,  154 => 97,  150 => 96,  146 => 95,  142 => 94,  138 => 93,  115 => 72,  113 => 69,  91 => 50,  63 => 25,  58 => 23,  54 => 22,  49 => 20,  45 => 19,  41 => 18,  37 => 17,  20 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">*/
/* <html>*/
/* <head>*/
/* <title>Book Store</title>*/
/* <!--[if lt IE 9]>*/
/* 	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>*/
/* <![endif]-->*/
/* <!--[if lt IE 9]>*/
/* 	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>*/
/* <![endif]-->*/
/* <meta http-equiv="cache-control" content="no-cache"></meta>*/
/* <meta charset="utf-8"></meta>*/
/* <meta name="viewport" content="initial-scale=1, maximum-scale=1"></meta>*/
/* <meta name="viewport" content="width=device-width"></meta>*/
/* <!-- Css Files Start -->*/
/* <link href="{{asset('css/style_personnalisé.css')}}" rel="stylesheet">*/
/* <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" /><!-- All css -->*/
/* <link href="{{asset('css/bs.css')}}" rel="stylesheet" type="text/css" /><!-- Bootstrap Css -->*/
/* <link rel="stylesheet" type="text/css" href="{{asset('css/main-slider.css')}}" /><!-- Main Slider Css -->*/
/* <!--[if lte IE 10]><link rel="stylesheet" type="text/css" href="css/customIE.css" /><![endif]-->*/
/* <link href="{{asset('css/font-awesome.css"')}}" rel="stylesheet" type="text/css" /><!-- Font Awesome Css -->*/
/* <link href="{{asset('css/font-awesome-ie7.css')}}" rel="stylesheet" type="text/css" /><!-- Font Awesome iE7 Css -->*/
/* <noscript>*/
/* <link rel="stylesheet" type="text/css" href="{{asset('css/noJS.css')}}" />*/
/* </noscript>*/
/* <!-- Css Files End -->*/
/*  <script*/
/*     src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMjKGV-a0gQYCeYJCrdDT2JThpg9U4ydk*/
/*     &callback=initMap">*/
/*     </script>*/
/*     <script type="text/javascript">*/
/*     var map;*/
/*     function initMap() {*/
/*     map = new google.maps.Map(document.getElementById('map'), {*/
/*     center: {lat: -34.397, lng: 150.644},*/
/*     zoom: 8*/
/*    });*/
/*     }*/
/*     </script>*/
/* </head>*/
/* <body>*/
/* <!-- Start Main Wrapper -->*/
/* <div class="wrapper"> */
/*   <!-- End Top Nav Bar -->*/
/*   <header id="main-header">*/
/*     <section class="container-fluid container">*/
/*       <section class="row-fluid">*/
/*         <section class="span4">*/
/*           <h1 id="logo"> <a href="index.html"><img src="{{asset('images/logo.png')}}" /></a> </h1>*/
/*         </section>*/
/*      */
/*       </section>*/
/*     </section>*/
/*     <!-- Start Main Nav Bar -->*/
/*     <nav id="nav">*/
/*       <div class="navbar navbar-inverse">*/
/*         <div class="navbar-inner">*/
/*           <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>*/
/*           */
/*           <!--/.nav-collapse -->*/
/*         </div>*/
/*         <!-- /.navbar-inner -->*/
/*       </div>*/
/*       <!-- /.navbar -->*/
/*     </nav>*/
/*     <!-- End Main Nav Bar -->*/
/*   </header>*/
/*   {% block contenu %}*/
/* */
/*   {% endblock %}*/
/*    */
/*  */
/*    */
/*   <!-- End Footer Top 2 -->*/
/*   <!-- Start Main Footer -->*/
/*   <footer id="main-footer">*/
/*     <section class="social-ico-bar">*/
/*       <section class="container">*/
/*         <section class="row-fluid">*/
/*           <article class="span6">*/
/*             <p>© Le 352 Esprit 2015 </p>*/
/*           </article>*/
/*           <article class="span6 copy-right">*/
/*             <p>Designed by <a href="">Le352.com</a></p>*/
/*           </article>*/
/*         </section>*/
/*       </section>*/
/*     </section>*/
/*   </footer>*/
/* <!-- End Main Wrapper -->*/
/* <!-- JS Files Start -->*/
/* <script type="text/javascript" src="{{asset('js/lib.js')}}"></script><!-- lib Js -->*/
/* <script type="text/javascript" src="{{asset('js/modernizr.js')}}"></script><!-- Modernizr -->*/
/* <script type="text/javascript" src="{{asset('js/easing.js')}}"></script><!-- Easing js -->*/
/* <script type="text/javascript" src="{{asset('js/bs.js')}}"></script><!-- Bootstrap -->*/
/* <script type="text/javascript" src="{{asset('js/bxslider.js')}}"></script><!-- BX Slider -->*/
/* <script type="text/javascript" src="{{asset('js/input-clear.js')}}"></script><!-- Input Clear -->*/
/* <script src="{{asset('js/range-slider.js')}}"></script><!-- Range Slider -->*/
/* <script src="{{asset('js/jquery.zoom.js')}}"></script><!-- Zoom Effect -->*/
/* <script type="text/javascript" src="{{asset('js/bookblock.js')}}"></script><!-- Flip Slider -->*/
/* <script type="text/javascript" src="{{asset('js/custom.js')}}"></script><!-- Custom js -->*/
/* <script type="text/javascript" src="{{asset('js/social.js')}}"></script><!-- Social Icons -->*/
/* <!-- JS Files End -->*/
/* <noscript>*/
/* <style>*/
/* 	#socialicons>a span { top: 0px; left: -100%; -webkit-transition: all 0.3s ease; -moz-transition: all 0.3s ease-in-out; -o-transition: all 0.3s ease-in-out; -ms-transition: all 0.3s ease-in-out; transition: all 0.3s 	ease-in-out;}*/
/* 	#socialicons>ahover div{left: 0px;}*/
/* 	</style>*/
/* </noscript>*/
/* <script type="text/javascript">*/
/*   /* <![CDATA[ *//* */
/*   $(document).ready(function() {*/
/*   $('.social_active').hoverdir( {} );*/
/* })*/
/* /* ]]> *//* */
/* </script>*/
/* </body>*/
/* </html>*/
/* */
