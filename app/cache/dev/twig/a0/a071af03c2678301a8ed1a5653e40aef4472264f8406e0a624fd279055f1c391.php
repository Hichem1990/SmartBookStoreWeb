<?php

/* SmartBookLecteurBundle:Utilisateur:ajouterreclamation.html.twig */
class __TwigTemplate_7193f800649d63f0fe023d15c9667ef45ec47bcc0098c8596b65c5d752f0e6e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 4
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout6.html.twig", "SmartBookLecteurBundle:Utilisateur:ajouterreclamation.html.twig", 4);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout6.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_contenu($context, array $blocks = array())
    {
        // line 8
        echo "
  ";
        // line 10
        echo "  <h1>Gestion des Utilisateurs</h1>
 
  <hr>
  <h2>Envoyer une Reclamtion  </h2>
 
  ";
        // line 15
        $this->loadTemplate("SmartBookLecteurBundle:Utilisateur:formulaire.html.twig", "SmartBookLecteurBundle:Utilisateur:ajouterreclamation.html.twig", 15)->display($context);
        // line 16
        echo " 
  
  
 
";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Utilisateur:ajouterreclamation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 16,  41 => 15,  34 => 10,  31 => 8,  28 => 7,  11 => 4,);
    }
}
/* {# src/SmartBook/LecteurBundle/Resources/views/Utilisateur/ajouter.html.twig#}*/
/*  */
/* */
/* {% extends "SmartBookLecteurBundle::layout6.html.twig" %}*/
/*  */
/*  */
/* {% block contenu %}*/
/* */
/*   {# On définit un sous-titre commun à toutes les pages du bundle, par exemple #}*/
/*   <h1>Gestion des Utilisateurs</h1>*/
/*  */
/*   <hr>*/
/*   <h2>Envoyer une Reclamtion  </h2>*/
/*  */
/*   {% include "SmartBookLecteurBundle:Utilisateur:formulaire.html.twig" %}*/
/*  */
/*   */
/*   */
/*  */
/* {% endblock %}*/
