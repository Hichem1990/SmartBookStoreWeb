<?php

/* SmartBookLecteurBundle:utilisateur:liste_libraires.html.twig */
class __TwigTemplate_d9f3c4327cc7d9ecf4759441f72f9d1b50694bea3995d5cab69c89c2aa9a9ee2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:utilisateur:liste_libraires.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "    <section id=\"content-holder\" class=\"container-fluid container\">
    <section class=\"row-fluid\">
     
    \t<div class=\"heading-bar\">
        \t<h2>Libraires</h2>         
           </div>
          ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["libraires"]) ? $context["libraires"] : $this->getContext($context, "libraires"))) - 1)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 11
            echo "          <section class=\"list-holder\">
           <article class=\"item-holder\">
              <div class=\"span2\">
                <a href=\"book-detail.html\"><img src=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(twig_join_filter(array(0 => "uploads/utilisateurs/", 1 => $this->getAttribute($this->getAttribute((isset($context["libraires"]) ? $context["libraires"] : $this->getContext($context, "libraires")), $context["i"], array(), "array"), "getImage", array(), "method")))), "html", null, true);
            echo "\" alt=\"Image07\" /></a> </div>
                   <div class=\"span10\">
                   <div class=\"title-bar\"><a href=\"\">";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["libraires"]) ? $context["libraires"] : $this->getContext($context, "libraires")), $context["i"], array(), "array"), "getNom", array(), "method"), "html", null, true);
            echo "</a></div>
                     <strong>Numero télephone</strong>
                      <p>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["libraires"]) ? $context["libraires"] : $this->getContext($context, "libraires")), $context["i"], array(), "array"), "getNumTel", array(), "method"), "html", null, true);
            echo "</p>
                      <strong>Email</strong>
                       <p>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["libraires"]) ? $context["libraires"] : $this->getContext($context, "libraires")), $context["i"], array(), "array"), "getEmail", array(), "method"), "html", null, true);
            echo "</p>          
                       <strong>Adresse</strong>
                       <p>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["libraires"]) ? $context["libraires"] : $this->getContext($context, "libraires")), $context["i"], array(), "array"), "getAdresse", array(), "method"), "html", null, true);
            echo "</p>
                       <a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_livre_afficheLibraire", array("id" => $this->getAttribute($this->getAttribute((isset($context["libraires"]) ? $context["libraires"] : $this->getContext($context, "libraires")), $context["i"], array(), "array"), "getId", array(), "method"))), "html", null, true);
            echo "\" class=\"shop-btn\">Ses livres</a>
                  </div>
                    
                </article>
          </section>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        </section>
        </section>
    ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:utilisateur:liste_libraires.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 29,  72 => 23,  68 => 22,  63 => 20,  58 => 18,  53 => 16,  48 => 14,  43 => 11,  39 => 10,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* {% block contenu %}*/
/*     <section id="content-holder" class="container-fluid container">*/
/*     <section class="row-fluid">*/
/*      */
/*     	<div class="heading-bar">*/
/*         	<h2>Libraires</h2>         */
/*            </div>*/
/*           {% for i in 0..libraires|length-1 %}*/
/*           <section class="list-holder">*/
/*            <article class="item-holder">*/
/*               <div class="span2">*/
/*                 <a href="book-detail.html"><img src="{{asset(['uploads/utilisateurs/',libraires[i].getImage()]|join )}}" alt="Image07" /></a> </div>*/
/*                    <div class="span10">*/
/*                    <div class="title-bar"><a href="">{{libraires[i].getNom()}}</a></div>*/
/*                      <strong>Numero télephone</strong>*/
/*                       <p>{{libraires[i].getNumTel()}}</p>*/
/*                       <strong>Email</strong>*/
/*                        <p>{{libraires[i].getEmail()}}</p>          */
/*                        <strong>Adresse</strong>*/
/*                        <p>{{libraires[i].getAdresse()}}</p>*/
/*                        <a href="{{path("smart_book_livre_afficheLibraire",{"id" :libraires[i].getId()})}}" class="shop-btn">Ses livres</a>*/
/*                   </div>*/
/*                     */
/*                 </article>*/
/*           </section>*/
/*           {% endfor %}*/
/*         </section>*/
/*         </section>*/
/*     {% endblock %}*/
/* */
