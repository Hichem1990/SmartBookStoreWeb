<?php

/* SmartBookLecteurBundle:Livre:list.html.twig */
class __TwigTemplate_aea959c32ef52ad8fad3d1fdea918f9b67cc7087ae5bedfc3366a32256eca44b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::compte_libraire.html.twig", "SmartBookLecteurBundle:Livre:list.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::compte_libraire.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "    <center><h1> Liste des Livres </h1></center>
    <center>
    <table border=1>
    <tr>
        <th>identifiant</th>
        <th>Nom du Livre</th>
        <th>Nom d'auteur du Livre</th>
        <th>Langue du Livre</th>
        <th>Prix du Livre</th>
        <th>Nombre des pages du Livre</th>
         <th>Date Edition</th>
         <th>Etat du Livre</th>
         <th>Categorie du Livre</th>
         <th>Modifier</th>
         <th>Supprimer</th>


    </tr>  
    ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["livres"]) ? $context["livres"] : $this->getContext($context, "livres")));
        foreach ($context['_seq'] as $context["_key"] => $context["Livre"]) {
            // line 23
            echo "        <tr>
           <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["Livre"], "idL", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["Livre"], "nomLivre", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["Livre"], "auteur", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["Livre"], "langue", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["Livre"], "prix", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["Livre"], "nbrPages", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["Livre"], "dateEdition", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["Livre"], "etat", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["Livre"], "categorie", array()), "html", null, true);
            echo "</td>
            <td>
            <a href=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_livre_Delete", array("idL" => $this->getAttribute($context["Livre"], "idL", array()))), "html", null, true);
            echo "\"> Delete </a>
            </td>
            <td>
             <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Smart_book_livre_Update", array("idL" => $this->getAttribute($context["Livre"], "idL", array()))), "html", null, true);
            echo "\">Modifier </a>
             </td>
             </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Livre'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "</table>
    </center>
 ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Livre:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 41,  101 => 37,  95 => 34,  90 => 32,  86 => 31,  82 => 30,  78 => 29,  74 => 28,  70 => 27,  66 => 26,  62 => 25,  58 => 24,  55 => 23,  51 => 22,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::compte_libraire.html.twig" %}*/
/* {% block contenu %}*/
/*     <center><h1> Liste des Livres </h1></center>*/
/*     <center>*/
/*     <table border=1>*/
/*     <tr>*/
/*         <th>identifiant</th>*/
/*         <th>Nom du Livre</th>*/
/*         <th>Nom d'auteur du Livre</th>*/
/*         <th>Langue du Livre</th>*/
/*         <th>Prix du Livre</th>*/
/*         <th>Nombre des pages du Livre</th>*/
/*          <th>Date Edition</th>*/
/*          <th>Etat du Livre</th>*/
/*          <th>Categorie du Livre</th>*/
/*          <th>Modifier</th>*/
/*          <th>Supprimer</th>*/
/* */
/* */
/*     </tr>  */
/*     {% for Livre in livres %}*/
/*         <tr>*/
/*            <td>{{Livre.idL }}</td>*/
/*             <td>{{Livre.nomLivre }}</td>*/
/*             <td>{{Livre.auteur }}</td>*/
/*             <td>{{Livre.langue}}</td>*/
/*             <td>{{Livre.prix}}</td>*/
/*             <td>{{Livre.nbrPages}}</td>*/
/*             <td>{{Livre.dateEdition}}</td>*/
/*             <td>{{Livre.etat}}</td>*/
/*             <td>{{Livre.categorie}}</td>*/
/*             <td>*/
/*             <a href="{{path('smart_book_livre_Delete' , {'idL':Livre.idL})}}"> Delete </a>*/
/*             </td>*/
/*             <td>*/
/*              <a href="{{path('Smart_book_livre_Update' , {'idL':Livre.idL})}}">Modifier </a>*/
/*              </td>*/
/*              </tr>*/
/*         {% endfor %}*/
/* </table>*/
/*     </center>*/
/*  {% endblock %}*/
/*     */
