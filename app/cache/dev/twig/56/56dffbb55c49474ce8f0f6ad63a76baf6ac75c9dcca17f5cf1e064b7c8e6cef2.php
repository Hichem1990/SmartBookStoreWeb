<?php

/* SmartBookLecteurBundle:Utilisateur:formulaire.html.twig */
class __TwigTemplate_14292181087baad0160928258e101faf312f53bc7dce6bcc78c98d6db8f099dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo " 

 
<div class=\"well\">
  <form method=\"post\" ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo ">
    ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <input type=\"submit\" class=\"btn btn-primary\" />
  </form>
</div>
";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Utilisateur:formulaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 7,  25 => 6,  19 => 2,);
    }
}
/* {# src/SmartBook/LecteurBundle/Resources/views/Utilisateur/formulaire.html.twig #}*/
/*  */
/* */
/*  */
/* <div class="well">*/
/*   <form method="post" {{ form_enctype(form) }}>*/
/*     {{ form_widget(form) }}*/
/*     <input type="submit" class="btn btn-primary" />*/
/*   </form>*/
/* </div>*/
/* */
