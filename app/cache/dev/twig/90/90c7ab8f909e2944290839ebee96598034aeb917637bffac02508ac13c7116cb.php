<?php

/* SmartBookLecteurBundle::map.html.twig */
class __TwigTemplate_70ec18ddca53e9885aedbac4efcb44ec1ef70e3c4f996c1b1d901f52b39ea898 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
  <head>
    <title>Book Store</title>
<!--[if lt IE 9]>
\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
<![endif]-->
<!--[if lt IE 9]>
\t<script src=\"http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js\"></script>
<![endif]-->
<!-- Css Files Start -->
<link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style_personnalisé.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- All css -->
<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bs.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Bootstrap Css -->
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main-slider.css"), "html", null, true);
        echo "\" /><!-- Main Slider Css -->
<!--[if lte IE 10]><link rel=\"stylesheet\" type=\"text/css\" href=\"css/customIE.css\" /><![endif]-->
<link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.css\""), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Font Awesome Css -->
<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome-ie7.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Font Awesome iE7 Css -->
<noscript>
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/noJS.css"), "html", null, true);
        echo "\" />
</noscript>
    <style type=\"text/css\">
      html, body { height: 100%; margin: 0; padding: 0; }
      #map { height: 100%; }
    </style>
    <script
      src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAMjKGV-a0gQYCeYJCrdDT2JThpg9U4ydk
      &callback=initMap\">
    </script>
    <script type=\"text/javascript\">
    
    function initMap() {
    var myLatLng = {lat:36.7948829, lng: 10.1432776};
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: myLatLng
     });
  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Vous étes ici!'
  });
    }
    
    function AfficheLibs() {
    //var lng = parseFloat(\"554,20\".replace(\",\", \".\"));
    var myLatLng = {lat:parseFloat(document.getElementById(\"longtitude\").value), lng:parseFloat(document.getElementById(\"latitude\").value)};
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: myLatLng
    
     });
    var infowindow = new google.maps.InfoWindow({
    content:\"Adresse:\"+document.getElementById(\"adresse\").value
     });
    var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    animation:google.maps.Animation.BOUNCE
    }); 
   google.maps.event.addListener(marker, 'click', function() {
   infowindow.open(map,marker);
    });
  
    }
    
    </script>
  </head>
  ";
        // line 70
        $this->displayBlock('contenu', $context, $blocks);
        // line 74
        echo "      <div id=\"map\"></div>
    
     <footer id=\"main-footer\">
    <section class=\"social-ico-bar\">
      <section class=\"container\">
        <section class=\"row-fluid\">
          <article class=\"span6\">
            <p>© Le 352 Esprit 2015 </p>
          </article>
          <article class=\"span6 copy-right\">
            <p>Designed by <a href=\"\">Le352.com</a></p>
          </article>
        </section>
      </section>
    </section>
  </footer>  
  </body>
  
</html>";
    }

    // line 70
    public function block_contenu($context, array $blocks = array())
    {
        // line 71
        echo "  
   
     ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle::map.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 71,  135 => 70,  113 => 74,  111 => 70,  59 => 21,  54 => 19,  50 => 18,  45 => 16,  41 => 15,  37 => 14,  33 => 13,  20 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*   <head>*/
/*     <title>Book Store</title>*/
/* <!--[if lt IE 9]>*/
/* 	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>*/
/* <![endif]-->*/
/* <!--[if lt IE 9]>*/
/* 	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>*/
/* <![endif]-->*/
/* <!-- Css Files Start -->*/
/* <link href="{{asset('css/style_personnalisé.css')}}" rel="stylesheet">*/
/* <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" /><!-- All css -->*/
/* <link href="{{asset('css/bs.css')}}" rel="stylesheet" type="text/css" /><!-- Bootstrap Css -->*/
/* <link rel="stylesheet" type="text/css" href="{{asset('css/main-slider.css')}}" /><!-- Main Slider Css -->*/
/* <!--[if lte IE 10]><link rel="stylesheet" type="text/css" href="css/customIE.css" /><![endif]-->*/
/* <link href="{{asset('css/font-awesome.css"')}}" rel="stylesheet" type="text/css" /><!-- Font Awesome Css -->*/
/* <link href="{{asset('css/font-awesome-ie7.css')}}" rel="stylesheet" type="text/css" /><!-- Font Awesome iE7 Css -->*/
/* <noscript>*/
/* <link rel="stylesheet" type="text/css" href="{{asset('css/noJS.css')}}" />*/
/* </noscript>*/
/*     <style type="text/css">*/
/*       html, body { height: 100%; margin: 0; padding: 0; }*/
/*       #map { height: 100%; }*/
/*     </style>*/
/*     <script*/
/*       src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMjKGV-a0gQYCeYJCrdDT2JThpg9U4ydk*/
/*       &callback=initMap">*/
/*     </script>*/
/*     <script type="text/javascript">*/
/*     */
/*     function initMap() {*/
/*     var myLatLng = {lat:36.7948829, lng: 10.1432776};*/
/*     var map = new google.maps.Map(document.getElementById('map'), {*/
/*     zoom: 4,*/
/*     center: myLatLng*/
/*      });*/
/*   var marker = new google.maps.Marker({*/
/*     position: myLatLng,*/
/*     map: map,*/
/*     title: 'Vous étes ici!'*/
/*   });*/
/*     }*/
/*     */
/*     function AfficheLibs() {*/
/*     //var lng = parseFloat("554,20".replace(",", "."));*/
/*     var myLatLng = {lat:parseFloat(document.getElementById("longtitude").value), lng:parseFloat(document.getElementById("latitude").value)};*/
/*     var map = new google.maps.Map(document.getElementById('map'), {*/
/*     zoom: 10,*/
/*     center: myLatLng*/
/*     */
/*      });*/
/*     var infowindow = new google.maps.InfoWindow({*/
/*     content:"Adresse:"+document.getElementById("adresse").value*/
/*      });*/
/*     var marker = new google.maps.Marker({*/
/*     position: myLatLng,*/
/*     map: map,*/
/*     animation:google.maps.Animation.BOUNCE*/
/*     }); */
/*    google.maps.event.addListener(marker, 'click', function() {*/
/*    infowindow.open(map,marker);*/
/*     });*/
/*   */
/*     }*/
/*     */
/*     </script>*/
/*   </head>*/
/*   {%  block contenu %}*/
/*   */
/*    */
/*      {% endblock %}*/
/*       <div id="map"></div>*/
/*     */
/*      <footer id="main-footer">*/
/*     <section class="social-ico-bar">*/
/*       <section class="container">*/
/*         <section class="row-fluid">*/
/*           <article class="span6">*/
/*             <p>© Le 352 Esprit 2015 </p>*/
/*           </article>*/
/*           <article class="span6 copy-right">*/
/*             <p>Designed by <a href="">Le352.com</a></p>*/
/*           </article>*/
/*         </section>*/
/*       </section>*/
/*     </section>*/
/*   </footer>  */
/*   </body>*/
/*   */
/* </html>*/
