<?php

/* SmartBookLecteurBundle:Livre:rechercher.html.twig */
class __TwigTemplate_df29ae095d5daed6236d59b6f20848eecb4d601e3c36134389472fafce1fd9ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::compte_libraire.html.twig", "SmartBookLecteurBundle:Livre:rechercher.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::compte_libraire.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_contenu($context, array $blocks = array())
    {
        // line 3
        echo "    <center><h1>Recherche Livre</h1></center>
    <center>
    <form method=\"POST\">
    Nom du Livre : <input type=\"text\" name=\"nomLivre\"><br>
    Etat du Livre : <input type=\"text\" name=\"etat\"><br>
    <input type=\"submit\" value=\"rechercher\">
   </form>
    </center>
";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Livre:rechercher.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "SmartBookLecteurBundle::compte_libraire.html.twig" %}*/
/* {% block contenu %}*/
/*     <center><h1>Recherche Livre</h1></center>*/
/*     <center>*/
/*     <form method="POST">*/
/*     Nom du Livre : <input type="text" name="nomLivre"><br>*/
/*     Etat du Livre : <input type="text" name="etat"><br>*/
/*     <input type="submit" value="rechercher">*/
/*    </form>*/
/*     </center>*/
/* {% endblock %}*/
