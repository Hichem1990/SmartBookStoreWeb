<?php

/* SmartBookLecteurBundle:Publication:new.html.twig */
class __TwigTemplate_da64f2ecb0e03e757427e5694878bc5ec09725fd8ce84a5e4d9f28af494df6b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:Publication:new.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "<body>
    
    <center> <h3><p> Contacter nous </p></h3></center>
    <p>";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "</p> 
    <hr> 
    <center>
    <form role=\"form\" id=\"fr\" method=\"POST\" action='' > 
        <table>
            <tr> 
           <td>NOM:</td>  
           <td>";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget');
        echo " </td>
           </tr>
            <tr> 
           <td>PRENOM:</td>  
           <td>";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget');
        echo " </td>
           </tr>
            <tr> 
           <td>TEL:</td>  
           <td>";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "tel", array()), 'widget');
        echo " </td>
           </tr>
            <tr> 
           <td>FROM:</td>  
           <td>";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "from", array()), 'widget');
        echo " </td>
           </tr>
            <tr> 
           <td>TEXT:</td>  
           <td>";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "text", array()), 'widget');
        echo " </td>
            </tr>
        </table>
            <br>
            <input type=\"submit\" value=\"VALIDER\"/>
            ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
    </form>
    </center>
</body>
";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Publication:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 35,  74 => 30,  67 => 26,  60 => 22,  53 => 18,  46 => 14,  36 => 7,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* {% block contenu %}*/
/* <body>*/
/*     */
/*     <center> <h3><p> Contacter nous </p></h3></center>*/
/*     <p>{{form_errors(form)}}</p> */
/*     <hr> */
/*     <center>*/
/*     <form role="form" id="fr" method="POST" action='' > */
/*         <table>*/
/*             <tr> */
/*            <td>NOM:</td>  */
/*            <td>{{form_widget(form.nom)}} </td>*/
/*            </tr>*/
/*             <tr> */
/*            <td>PRENOM:</td>  */
/*            <td>{{form_widget(form.prenom)}} </td>*/
/*            </tr>*/
/*             <tr> */
/*            <td>TEL:</td>  */
/*            <td>{{form_widget(form.tel)}} </td>*/
/*            </tr>*/
/*             <tr> */
/*            <td>FROM:</td>  */
/*            <td>{{form_widget(form.from)}} </td>*/
/*            </tr>*/
/*             <tr> */
/*            <td>TEXT:</td>  */
/*            <td>{{form_widget(form.text)}} </td>*/
/*             </tr>*/
/*         </table>*/
/*             <br>*/
/*             <input type="submit" value="VALIDER"/>*/
/*             {{form_rest(form)}}*/
/*     </form>*/
/*     </center>*/
/* </body>*/
/* {% endblock %}*/
