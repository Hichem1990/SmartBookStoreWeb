<?php

/* SmartBookLecteurBundle:Utilisateur:listAdmin.html.twig */
class __TwigTemplate_a482fe962c730e59d970d677e2f4328fe6a18691e13892135c2c440ef09cf343 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout6.html.twig", "SmartBookLecteurBundle:Utilisateur:listAdmin.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout6.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_contenu($context, array $blocks = array())
    {
        echo " 
<h1>liste des utilisateurs</h1>

<table border=\"1\">
    <tr>
        <th> Id </th>

        <th> nom </th>
        <th> username </th>

        <th> prenom </th>
        <th> sexe </th>\t
        <th> adresse </th>
        <th> numTel </th>
        <th> dateNaissance </th>\t

        <th> etatCompte </th>\t
        <th> etatInscription</th>\t
        <th> nbrSignale</th>\t
        
        

        <th> Réclamtion Administrative </th>
    </tr>
    
    ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["utilisateurs"]) ? $context["utilisateurs"] : $this->getContext($context, "utilisateurs")));
        foreach ($context['_seq'] as $context["_key"] => $context["utilisateur"]) {
            // line 30
            echo "    <tr>
        <th>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "id", array()), "html", null, true);
            echo "</th>

        <th>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "nom", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "username", array()), "html", null, true);
            echo "</th>

        <th>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "prenom", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "sexe", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "adresse", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "numTel", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "dateNaissance", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "etatCompte", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "etatInscription", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["utilisateur"], "nbrSignale", array()), "html", null, true);
            echo "</th>
  <th><a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_reclame_utilisateur", array("id" => $this->getAttribute($context["utilisateur"], "id", array()))), "html", null, true);
            echo "\">Reclamer</a> </th>





    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['utilisateur'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "
</table>
";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Utilisateur:listAdmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 52,  110 => 44,  106 => 43,  102 => 42,  98 => 41,  94 => 40,  90 => 39,  86 => 38,  82 => 37,  78 => 36,  73 => 34,  69 => 33,  64 => 31,  61 => 30,  57 => 29,  28 => 4,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout6.html.twig" %}*/
/* */
/* {%  block contenu  %} */
/* <h1>liste des utilisateurs</h1>*/
/* */
/* <table border="1">*/
/*     <tr>*/
/*         <th> Id </th>*/
/* */
/*         <th> nom </th>*/
/*         <th> username </th>*/
/* */
/*         <th> prenom </th>*/
/*         <th> sexe </th>	*/
/*         <th> adresse </th>*/
/*         <th> numTel </th>*/
/*         <th> dateNaissance </th>	*/
/* */
/*         <th> etatCompte </th>	*/
/*         <th> etatInscription</th>	*/
/*         <th> nbrSignale</th>	*/
/*         */
/*         */
/* */
/*         <th> Réclamtion Administrative </th>*/
/*     </tr>*/
/*     */
/*     {% for utilisateur in utilisateurs %}*/
/*     <tr>*/
/*         <th>{{utilisateur.id}}</th>*/
/* */
/*         <th>{{utilisateur.nom}}</th>*/
/*         <th>{{utilisateur.username}}</th>*/
/* */
/*         <th>{{utilisateur.prenom}}</th>*/
/*         <th>{{utilisateur.sexe}}</th>*/
/*         <th>{{utilisateur.adresse}}</th>*/
/*         <th>{{utilisateur.numTel}}</th>*/
/*         <th>{{utilisateur.dateNaissance}}</th>*/
/*         <th>{{utilisateur.etatCompte}}</th>*/
/*         <th>{{utilisateur.etatInscription}}</th>*/
/*         <th>{{utilisateur.nbrSignale}}</th>*/
/*   <th><a href="{{path("smart_book_reclame_utilisateur", {'id':utilisateur.id })}}">Reclamer</a> </th>*/
/* */
/* */
/* */
/* */
/* */
/*     </tr>*/
/*     {% endfor %}*/
/* */
/* </table>*/
/* {% endblock %}*/
