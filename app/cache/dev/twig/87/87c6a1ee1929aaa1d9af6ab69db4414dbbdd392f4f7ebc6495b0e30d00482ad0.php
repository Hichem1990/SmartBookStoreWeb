<?php

/* SmartBookLecteurBundle:Utilisateur:resultat.html.twig */
class __TwigTemplate_cfc71a668256a86b52f9098a84c403e19bee0e3ff1d804e2f99db1374df91607 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout6.html.twig", "SmartBookLecteurBundle:Utilisateur:resultat.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout6.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "    <center>
    <h1>Résultat recherche</h1>
    <table border=\"1\"> 
        <tr>
           <th> Id </th>

        <th> nom </th>
        <th> username </th>

        <th> prenom </th>
        <th> sexe </th>\t
        <th> adresse </th>
        <th> numTel </th>
        <th> dateNaissance </th>\t

        <th> etatCompte </th>\t
        <th> etatInscription</th>\t
        <th> nbrSignale</th>\t
        
        
    </tr>
        </tr> 
        ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["utilisateur"]) ? $context["utilisateur"] : $this->getContext($context, "utilisateur")));
        foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
            echo " 
        <th>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "id", array()), "html", null, true);
            echo "</th>

        <th>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "nom", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "username", array()), "html", null, true);
            echo "</th>

        <th>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "prenom", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "sexe", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "adresse", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "numTel", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "dateNaissance", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "etatCompte", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "etatInscription", array()), "html", null, true);
            echo "</th>
        <th>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "nbrSignale", array()), "html", null, true);
            echo "</th>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "        </table> 
    </center>
";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Utilisateur:resultat.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 42,  103 => 39,  99 => 38,  95 => 37,  91 => 36,  87 => 35,  83 => 34,  79 => 33,  75 => 32,  70 => 30,  66 => 29,  61 => 27,  55 => 26,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #} */
/* {% extends "SmartBookLecteurBundle::layout6.html.twig" %}*/
/* {% block contenu %}*/
/*     <center>*/
/*     <h1>Résultat recherche</h1>*/
/*     <table border="1"> */
/*         <tr>*/
/*            <th> Id </th>*/
/* */
/*         <th> nom </th>*/
/*         <th> username </th>*/
/* */
/*         <th> prenom </th>*/
/*         <th> sexe </th>	*/
/*         <th> adresse </th>*/
/*         <th> numTel </th>*/
/*         <th> dateNaissance </th>	*/
/* */
/*         <th> etatCompte </th>	*/
/*         <th> etatInscription</th>	*/
/*         <th> nbrSignale</th>	*/
/*         */
/*         */
/*     </tr>*/
/*         </tr> */
/*         {% for e in utilisateur %} */
/*         <th>{{e.id}}</th>*/
/* */
/*         <th>{{e.nom}}</th>*/
/*         <th>{{e.username}}</th>*/
/* */
/*         <th>{{e.prenom}}</th>*/
/*         <th>{{e.sexe}}</th>*/
/*         <th>{{e.adresse}}</th>*/
/*         <th>{{e.numTel}}</th>*/
/*         <th>{{e.dateNaissance}}</th>*/
/*         <th>{{e.etatCompte}}</th>*/
/*         <th>{{e.etatInscription}}</th>*/
/*         <th>{{e.nbrSignale}}</th>*/
/* */
/*         {% endfor %}*/
/*         </table> */
/*     </center>*/
/* {% endblock %}*/
