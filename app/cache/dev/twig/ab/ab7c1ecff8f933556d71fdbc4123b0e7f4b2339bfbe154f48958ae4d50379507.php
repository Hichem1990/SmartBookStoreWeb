<?php

/* SmartBookLecteurBundle:Livre:ajout.html.twig */
class __TwigTemplate_902e7694d78aa77e1737aa9a4b8f3a328a6464454592a7a89b24d0f5d4a4cfc4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::compte_libraire.html.twig", "SmartBookLecteurBundle:Livre:ajout.html.twig", 3);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::compte_libraire.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_contenu($context, array $blocks = array())
    {
        // line 6
        echo "    <center>
    <h2>Formulaire d'ajout d'un Livre </h2>
    </center>
    <center>
    <form method=\"POST\" action=\"\"> 
   
        <table border=\"0\" cellpadding=\"15\">
   <tr>
       <td><label><i>Nom du Livre</i></label></td>
       <td><input name=\"noml\" type=\"text\"/></td>
   </tr>
   <tr>
       <td><label><i>Image du Livre</i></label></td>
       <td><input name=\"image\" type=\"file\" style=\"width:310px\"/></td>
   </tr>
   <tr>
       <td><label><i>Nom d'auteur du Livre</i></label></td>
       <td><input name=\"auteur\" type=\"text\" /></td>
   </tr>
   <tr>
       <td><label><i>Langue du Livre</i></label></td>
      <td>
          
      <select name=\"langue\" style=\"width:310px\">
       <option value=\"Arabe\" >Arabe</option> 
       <option value=\"Francais\" >Francais</option>
       <option value=\"Anglais\">Anglais</option>
       </select>
          </td>
       
   <tr>
       <td><label><i>Prix du Livre</i></label></td>
       <td><input  name=\"prix\" type=\"number\"/></td>
   </tr>
   <tr>
       <td><label><i>Nombre des pages du Livre</i></label></td>
       <td><input name=\"nbrp\" type=\"number\"/></td>
   </tr>
   <tr>
       <td><label><i>Date Edition</i></label></td>
       <td><input name=\"date\" type=\"date\" style=\"width:310px\"/></td>
   </tr>
   <tr>
       <td><label><i>Etat du Livre</i></label></td>
       <td> 
      <select name=\"etat\" style=\"width:310px\">
       <option value=\"Bonne\" selected>Bonne</option> 
       <option value=\"mauvaise\" selected>Mauvaise</option>
       </select>
       </td>
   </tr>
   <tr>
       <td><label><i>Categorie du Livre</i></label></td>
       <td>
           <select name=\"categorie\" style=\"width:310px\">
       <option value=\"Sport\" selected>Sport</option> 
       <option value=\"Cuisine\" selected>Cuisine</option>
       <option value=\"Culture\" selected>Culture</option>
       <option value=\"Art\" selected>Art</option>
       <option value=\"Santé\" selected>Santé</option>
       <option value=\"Sciences\" selected>Sciences</option>
       
       </select>
       </td>
   </tr>
  
   <tr>
       <td>
     <input type='submit' value=\"Ajouter\" style=\"width:130px\"/>
       </td>
   <td>
     <input type='reset' value=\"Annuler\" style=\"width:130px\"/>
       </td><br>
    </tr>
    
    </table>
</center>     
    ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Livre:ajout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 6,  28 => 5,  11 => 3,);
    }
}
/* {# empty Twig template #}*/
/* {# Ressources/views/Livre/ajout.thml.twig #}*/
/* {% extends "SmartBookLecteurBundle::compte_libraire.html.twig" %}*/
/* */
/* {% block contenu %}*/
/*     <center>*/
/*     <h2>Formulaire d'ajout d'un Livre </h2>*/
/*     </center>*/
/*     <center>*/
/*     <form method="POST" action=""> */
/*    */
/*         <table border="0" cellpadding="15">*/
/*    <tr>*/
/*        <td><label><i>Nom du Livre</i></label></td>*/
/*        <td><input name="noml" type="text"/></td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><i>Image du Livre</i></label></td>*/
/*        <td><input name="image" type="file" style="width:310px"/></td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><i>Nom d'auteur du Livre</i></label></td>*/
/*        <td><input name="auteur" type="text" /></td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><i>Langue du Livre</i></label></td>*/
/*       <td>*/
/*           */
/*       <select name="langue" style="width:310px">*/
/*        <option value="Arabe" >Arabe</option> */
/*        <option value="Francais" >Francais</option>*/
/*        <option value="Anglais">Anglais</option>*/
/*        </select>*/
/*           </td>*/
/*        */
/*    <tr>*/
/*        <td><label><i>Prix du Livre</i></label></td>*/
/*        <td><input  name="prix" type="number"/></td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><i>Nombre des pages du Livre</i></label></td>*/
/*        <td><input name="nbrp" type="number"/></td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><i>Date Edition</i></label></td>*/
/*        <td><input name="date" type="date" style="width:310px"/></td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><i>Etat du Livre</i></label></td>*/
/*        <td> */
/*       <select name="etat" style="width:310px">*/
/*        <option value="Bonne" selected>Bonne</option> */
/*        <option value="mauvaise" selected>Mauvaise</option>*/
/*        </select>*/
/*        </td>*/
/*    </tr>*/
/*    <tr>*/
/*        <td><label><i>Categorie du Livre</i></label></td>*/
/*        <td>*/
/*            <select name="categorie" style="width:310px">*/
/*        <option value="Sport" selected>Sport</option> */
/*        <option value="Cuisine" selected>Cuisine</option>*/
/*        <option value="Culture" selected>Culture</option>*/
/*        <option value="Art" selected>Art</option>*/
/*        <option value="Santé" selected>Santé</option>*/
/*        <option value="Sciences" selected>Sciences</option>*/
/*        */
/*        </select>*/
/*        </td>*/
/*    </tr>*/
/*   */
/*    <tr>*/
/*        <td>*/
/*      <input type='submit' value="Ajouter" style="width:130px"/>*/
/*        </td>*/
/*    <td>*/
/*      <input type='reset' value="Annuler" style="width:130px"/>*/
/*        </td><br>*/
/*     </tr>*/
/*     */
/*     </table>*/
/* </center>     */
/*     {% endblock %}*/
/*     */
/* */
/* */
