<?php

/* SmartBookLecteurBundle:Utilisateur:search.html.twig */
class __TwigTemplate_8b08d5fef3040fdeeee4f8be8725bc02581942b1b7c33e905cf6c9dd1fe8337e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout6.html.twig", "SmartBookLecteurBundle:Utilisateur:search.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout6.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        echo " 
    <center>
    <h1>Recherche Utilisateur</h1>
    <form method=\"POST\"> id : <input type=\"text\" name=\"id\"> 
        <input type=\"submit\" value=\"recherche\"> </form></center>
";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Utilisateur:search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #} */
/* {% extends "SmartBookLecteurBundle::layout6.html.twig" %} */
/* {% block contenu %} */
/*     <center>*/
/*     <h1>Recherche Utilisateur</h1>*/
/*     <form method="POST"> id : <input type="text" name="id"> */
/*         <input type="submit" value="recherche"> </form></center>*/
/* {% endblock %}*/
