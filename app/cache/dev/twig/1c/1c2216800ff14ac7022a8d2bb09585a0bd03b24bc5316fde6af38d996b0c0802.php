<?php

/* SmartBookLecteurBundle:Evenement:affichepdf.html.twig */
class __TwigTemplate_74e4fd5fa426a1a3359cf629aa3a9d26aad72b8062114097a33265a0a6c78f37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:Evenement:affichepdf.html.twig", 3);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_contenu($context, array $blocks = array())
    {
        // line 5
        echo "    
        <h1>";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["evenement"]) ? $context["evenement"] : $this->getContext($context, "evenement")), "dateevent", array()), "html", null, true);
        echo "</H1>
        <h1>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["evenement"]) ? $context["evenement"] : $this->getContext($context, "evenement")), "titre", array()), "html", null, true);
        echo "</H1>
 
 ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Evenement:affichepdf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 7,  34 => 6,  31 => 5,  28 => 4,  11 => 3,);
    }
}
/* {# empty Twig template #}*/
/* */
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* {% block contenu %}*/
/*     */
/*         <h1>{{evenement.dateevent}}</H1>*/
/*         <h1>{{evenement.titre}}</H1>*/
/*  */
/*  {% endblock %}*/
