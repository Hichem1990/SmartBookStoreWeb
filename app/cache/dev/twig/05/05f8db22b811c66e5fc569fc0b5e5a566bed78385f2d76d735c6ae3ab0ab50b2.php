<?php

/* SmartBookLecteurBundle:Evenement:gerer_evenements.html.twig */
class __TwigTemplate_388faa6d81c4dee8d5b7de250f25260afe6b6d93b78cf7f32f0cad0c731ca765 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:Evenement:gerer_evenements.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "     ";
        if (((isset($context["evenements"]) ? $context["evenements"] : $this->getContext($context, "evenements")) == null)) {
            // line 5
            echo "        <center><h1 class=\"titre_annonce\">Aucune annonce est publié</h1> </center>
    ";
        } else {
            // line 7
            echo "    <center><h1>liste des evenement</h1></center>
<center>
<table border=\"1\">
    <tr>
        
        <th>Date evenement</th>
        <th>Titre </th>
        <th>Organisateur</th>
        <th>Duree</th>
        <th>Lieu</th>
        <th>Description</th>               
        <th>Supprimer</th>     
        <th>Modifier</th>
        <th>Genere PDF</th>
       
               
    </tr>
    ";
            // line 24
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["evenements"]) ? $context["evenements"] : $this->getContext($context, "evenements")));
            foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
                // line 25
                echo "    <tr>
        
        <td>";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "dateEvent", array()), "html", null, true);
                echo "</td>
        <td>";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "titre", array()), "html", null, true);
                echo "</td>
        <td>";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["e"], "utilisateur", array()), "getNom", array(), "method"), "html", null, true);
                echo "</td>
        <td>";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "dureeEve", array()), "html", null, true);
                echo "</td>
        <td>";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "lieuEve", array()), "html", null, true);
                echo "</td>
        <td>";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($context["e"], "descriptionEve", array()), "html", null, true);
                echo "</td>  
        <td>
            <a href=\"";
                // line 34
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_delete", array("id_e" => $this->getAttribute($context["e"], "idE", array()), "userid" => 38)), "html", null, true);
                echo "\"><button>Supprimer</button> </a>
        </td>
                       
         <td>
             <a href=\"";
                // line 38
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_update", array("id_e" => $this->getAttribute($context["e"], "idE", array()), "userid" => 38)), "html", null, true);
                echo "\"><button>Modifier</button> </a>
         </td>
         <td>
             <a href=\"";
                // line 41
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_pdf", array("id" => $this->getAttribute($context["e"], "idE", array()))), "html", null, true);
                echo "\" ><button>PDF</button> </a>
         </td>
         
         
        
    </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "    ";
        }
        // line 49
        echo "</table>
</center>


        

        
          
  ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Evenement:gerer_evenements.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 49,  116 => 48,  103 => 41,  97 => 38,  90 => 34,  85 => 32,  81 => 31,  77 => 30,  73 => 29,  69 => 28,  65 => 27,  61 => 25,  57 => 24,  38 => 7,  34 => 5,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* {% block contenu %}*/
/*      {% if evenements ==null %}*/
/*         <center><h1 class="titre_annonce">Aucune annonce est publié</h1> </center>*/
/*     {% else %}*/
/*     <center><h1>liste des evenement</h1></center>*/
/* <center>*/
/* <table border="1">*/
/*     <tr>*/
/*         */
/*         <th>Date evenement</th>*/
/*         <th>Titre </th>*/
/*         <th>Organisateur</th>*/
/*         <th>Duree</th>*/
/*         <th>Lieu</th>*/
/*         <th>Description</th>               */
/*         <th>Supprimer</th>     */
/*         <th>Modifier</th>*/
/*         <th>Genere PDF</th>*/
/*        */
/*                */
/*     </tr>*/
/*     {% for e in evenements %}*/
/*     <tr>*/
/*         */
/*         <td>{{e.dateEvent}}</td>*/
/*         <td>{{e.titre}}</td>*/
/*         <td>{{e.utilisateur.getNom()}}</td>*/
/*         <td>{{e.dureeEve}}</td>*/
/*         <td>{{e.lieuEve}}</td>*/
/*         <td>{{e.descriptionEve}}</td>  */
/*         <td>*/
/*             <a href="{{path('smart_book_lecteur_delete' , {'id_e' : e.idE,"userid":38})}}"><button>Supprimer</button> </a>*/
/*         </td>*/
/*                        */
/*          <td>*/
/*              <a href="{{path('smart_book_lecteur_update' , {'id_e' : e.idE,"userid":38})}}"><button>Modifier</button> </a>*/
/*          </td>*/
/*          <td>*/
/*              <a href="{{path('smart_book_lecteur_pdf', {'id' : e.idE})}}" ><button>PDF</button> </a>*/
/*          </td>*/
/*          */
/*          */
/*         */
/*     </tr>*/
/*     {% endfor %}*/
/*     {% endif %}*/
/* </table>*/
/* </center>*/
/* */
/* */
/*         */
/* */
/*         */
/*           */
/*   {% endblock %}*/
/*   */
