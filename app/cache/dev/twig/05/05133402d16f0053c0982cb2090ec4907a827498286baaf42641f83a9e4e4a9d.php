<?php

/* SmartBookLecteurBundle:librairie:affiche_librairie.html.twig */
class __TwigTemplate_edaf7c483ec52d30c5d555a8deab5f8819fb7ff8bc4bca585ce850e0395cdd4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::map.html.twig", "SmartBookLecteurBundle:librairie:affiche_librairie.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::map.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "     
 <body onload=\"AfficheLibs()\">
  <div class=\"wrapper\">
  <!-- Start Main Header -->
  <!-- Start Top Nav Bar -->
  <section class=\"top-nav-bar\">
    <section class=\"container-fluid container\">
      <section class=\"row-fluid\">
        <section class=\"span6\">
                <ul class=\"top-nav\">
            <li><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("smart_book_lecteur_Affichage");
        echo "\" class=\"active\">Accueil</a></li>
            <li><a href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_GererPub", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
        echo "\">Mes annonces</a></li>
             <li><a href=\"\">Deposer annonce</a>
             <ul id=\"sous_liste\">
\t\t<li><a href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_Deposer", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()), "type" => "echange")), "html", null, true);
        echo "\">Annonce d'echange</a></li>
\t\t<li><a href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_Deposer", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()), "type" => "vente")), "html", null, true);
        echo "\">Annonce de vente</a></li>
\t\t<li><a href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_Deposer", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()), "type" => "don")), "html", null, true);
        echo "\">Annonce de Don</a></li>
\t\t<li><a href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_Deposer", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()), "type" => "achat")), "html", null, true);
        echo "\">Annonce d'achat</a></li>
             </ul>
           </li>
             <li><a href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("smart_book_lecteur_AfficherLibraire");
        echo "\">Libraires</a></li>
            <li><a href=\"\">Evenements</a>
                 <ul id=\"sous_liste_evenement\">
\t\t<li><a href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_add", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
        echo "\">Lancer evenement</a></li>
\t\t<li><a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("smart_book_lecteur_affiche");
        echo "\">Consulter les evenements</a></li>
\t\t<li><a href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_GererEven", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
        echo "\">Gérer les evenements</a></li>
\t\t
             </ul>
            </li>
            <li><a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("smart_book_lecteur_Localisation");
        echo "\">Localisation</a></li>
                       
          </ul>
        </section>
        <section class=\"span6 e-commerce-list\">
          <ul>
            <li>Bienvenue! <a href=\"checkout.html\">Login</a> or <a href=\"checkout.html\">Creer compte</a></li>         
          </ul>
          <div class=\"c-btn\"> <a href=\"cart.html\" class=\"cart-btn\">Cart</a>
            <div class=\"btn-group\">
              <button data-toggle=\"dropdown\" class=\"btn btn-mini dropdown-toggle\">0 item(s) - \$0.00<span class=\"caret\"></span></button>
              <ul class=\"dropdown-menu\">
                <li><a href=\"#\">Action</a></li>
                <li><a href=\"#\">Another action</a></li>
                <li><a href=\"#\">Something else here</a></li>
              </ul>
            </div>
          </div>
        </section>
      </section>
    </section>
  </section>
      </div>
    <input type=\"hidden\" id=\"longtitude\" value=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["librairie"]) ? $context["librairie"] : $this->getContext($context, "librairie")), "getLogtitude", array(), "method"), "html", null, true);
        echo "\" />
    <input type=\"hidden\" id=\"latitude\" value=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["librairie"]) ? $context["librairie"] : $this->getContext($context, "librairie")), "getLatitude", array(), "method"), "html", null, true);
        echo "\" /> 
    <input type=\"hidden\" id=\"adresse\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["librairie"]) ? $context["librairie"] : $this->getContext($context, "librairie")), "getAdresse", array(), "method"), "html", null, true);
        echo "\" /> 
    ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:librairie:affiche_librairie.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 58,  122 => 57,  118 => 56,  92 => 33,  85 => 29,  81 => 28,  77 => 27,  71 => 24,  65 => 21,  61 => 20,  57 => 19,  53 => 18,  47 => 15,  43 => 14,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {%extends "SmartBookLecteurBundle::map.html.twig" %}*/
/* {% block contenu %}*/
/*      */
/*  <body onload="AfficheLibs()">*/
/*   <div class="wrapper">*/
/*   <!-- Start Main Header -->*/
/*   <!-- Start Top Nav Bar -->*/
/*   <section class="top-nav-bar">*/
/*     <section class="container-fluid container">*/
/*       <section class="row-fluid">*/
/*         <section class="span6">*/
/*                 <ul class="top-nav">*/
/*             <li><a href="{{path("smart_book_lecteur_Affichage")}}" class="active">Accueil</a></li>*/
/*             <li><a href="{{path("smart_book_lecteur_GererPub",{"id" :app.user.id})}}">Mes annonces</a></li>*/
/*              <li><a href="">Deposer annonce</a>*/
/*              <ul id="sous_liste">*/
/* 		<li><a href="{{path("smart_book_lecteur_Deposer",{"id" :app.user.id,"type":"echange"})}}">Annonce d'echange</a></li>*/
/* 		<li><a href="{{path("smart_book_lecteur_Deposer",{"id" :app.user.id,"type":"vente"})}}">Annonce de vente</a></li>*/
/* 		<li><a href="{{path("smart_book_lecteur_Deposer",{"id" :app.user.id,"type":"don"})}}">Annonce de Don</a></li>*/
/* 		<li><a href="{{path("smart_book_lecteur_Deposer",{"id" :app.user.id,"type":"achat"})}}">Annonce d'achat</a></li>*/
/*              </ul>*/
/*            </li>*/
/*              <li><a href="{{path("smart_book_lecteur_AfficherLibraire")}}">Libraires</a></li>*/
/*             <li><a href="">Evenements</a>*/
/*                  <ul id="sous_liste_evenement">*/
/* 		<li><a href="{{path("smart_book_lecteur_add",{"id" :app.user.id})}}">Lancer evenement</a></li>*/
/* 		<li><a href="{{path("smart_book_lecteur_affiche")}}">Consulter les evenements</a></li>*/
/* 		<li><a href="{{path("smart_book_lecteur_GererEven",{"id" :app.user.id})}}">Gérer les evenements</a></li>*/
/* 		*/
/*              </ul>*/
/*             </li>*/
/*             <li><a href="{{path("smart_book_lecteur_Localisation")}}">Localisation</a></li>*/
/*                        */
/*           </ul>*/
/*         </section>*/
/*         <section class="span6 e-commerce-list">*/
/*           <ul>*/
/*             <li>Bienvenue! <a href="checkout.html">Login</a> or <a href="checkout.html">Creer compte</a></li>         */
/*           </ul>*/
/*           <div class="c-btn"> <a href="cart.html" class="cart-btn">Cart</a>*/
/*             <div class="btn-group">*/
/*               <button data-toggle="dropdown" class="btn btn-mini dropdown-toggle">0 item(s) - $0.00<span class="caret"></span></button>*/
/*               <ul class="dropdown-menu">*/
/*                 <li><a href="#">Action</a></li>*/
/*                 <li><a href="#">Another action</a></li>*/
/*                 <li><a href="#">Something else here</a></li>*/
/*               </ul>*/
/*             </div>*/
/*           </div>*/
/*         </section>*/
/*       </section>*/
/*     </section>*/
/*   </section>*/
/*       </div>*/
/*     <input type="hidden" id="longtitude" value="{{librairie. getLogtitude()}}" />*/
/*     <input type="hidden" id="latitude" value="{{librairie.getLatitude()}}" /> */
/*     <input type="hidden" id="adresse" value="{{librairie.getAdresse()}}" /> */
/*     {% endblock %}*/
/*   */
/*    */
