<?php

/* SmartBookLecteurBundle:publication:detail_publication.html.twig */
class __TwigTemplate_7e18af5ec195ec95fb75c72697689ea5bd01d4562a70f585d08e82241bcf8d3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:publication:detail_publication.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "     <section class=\"b-detail-holder\">
         <article class=\"title-holder\">
             <div class=\"span6\">
               <h4><strong>Publier par </strong>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getUtilisateur", array(), "method"), "getNom", array(), "method"), "html", null, true);
        echo "</h4>
                <h4><strong>De </strong>";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getUtilisateur", array(), "method"), "getAdresse", array(), "method"), "html", null, true);
        echo "</h4>
           </div>
       <div class=\"span6 book-d-nav\">
             <ul>                        \t
               <li><i class=\"icon-envelope\"></i><a href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_Email", array("touser" => $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getUtilisateur", array(), "method"), "getEmail", array(), "method"))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getUtilisateur", array(), "method"), "getEmail", array(), "method"), "html", null, true);
        echo "</a></li>
            </ul>
        </div>
            
     </article>
       <div class=\"book-i-caption\">
                <!-- Strat Book Image Section -->
          <div class=\"span6 b-img-holder\">
       <span class='zoom' id='ex1'><img src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(twig_join_filter(array(0 => "uploads/livres/", 1 => $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getLivre", array(), "method"), "getImageL", array(), "method")))), "html", null, true);
        echo "\" height=\"219\" width=\"300\" id='jack' alt=''/></span>
      </div>
                <!-- Strat Book Image Section -->
                
                <!-- Strat Book Overview Section -->    
        <div class=\"span6\">
      <strong class=\"title\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getLivre", array(), "method"), "getNomLivre", array(), "method"), "html", null, true);
        echo "</strong>
       \t<p>";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getPresentation", array(), "method"), "html", null, true);
        echo "</p>  
        <strong class=\"title\">Auteur :</strong>
        <p>";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getLivre", array(), "method"), "getAuteur", array(), "method"), "html", null, true);
        echo "</p> 
        <strong class=\"title\">Langue :</strong>
        <p>";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getLivre", array(), "method"), "getLangue", array(), "method"), "html", null, true);
        echo "</p> 
         <strong class=\"title\">Etat :</strong>
        <p>";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getLivre", array(), "method"), "getEtat", array(), "method"), "html", null, true);
        echo "</p>       
         <strong class=\"title\">Prix :</strong>
        <p>";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getLivre", array(), "method"), "getPrix", array(), "method"), "html", null, true);
        echo "</p> 
         <strong class=\"title\">Nombre des pages :</strong>
        <p>";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getLivre", array(), "method"), "getNbrPages", array(), "method"), "html", null, true);
        echo "</p> 
         <strong class=\"title\">Type publication :</strong>
        <p>";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pub"]) ? $context["pub"] : $this->getContext($context, "pub")), "getType", array(), "method"), "html", null, true);
        echo "</p> 
        <strong class=\"title\">";
        // line 40
        echo $this->env->getExtension('nomaya_social_bar')->getSocialButtons();
        echo "
                 </strong>
        
                      
                   </div>
                <!-- End Book Overview Section -->
                </div>
      \t\t  </section>       
";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:publication:detail_publication.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 40,  103 => 39,  98 => 37,  93 => 35,  88 => 33,  83 => 31,  78 => 29,  73 => 27,  69 => 26,  60 => 20,  47 => 12,  40 => 8,  36 => 7,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* {% block contenu %}*/
/*      <section class="b-detail-holder">*/
/*          <article class="title-holder">*/
/*              <div class="span6">*/
/*                <h4><strong>Publier par </strong>{{pub.getUtilisateur().getNom()}}</h4>*/
/*                 <h4><strong>De </strong>{{pub.getUtilisateur().getAdresse()}}</h4>*/
/*            </div>*/
/*        <div class="span6 book-d-nav">*/
/*              <ul>                        	*/
/*                <li><i class="icon-envelope"></i><a href="{{path("smart_book_lecteur_Email",{"touser" :pub.getUtilisateur().getEmail()})}}">{{pub.getUtilisateur().getEmail()}}</a></li>*/
/*             </ul>*/
/*         </div>*/
/*             */
/*      </article>*/
/*        <div class="book-i-caption">*/
/*                 <!-- Strat Book Image Section -->*/
/*           <div class="span6 b-img-holder">*/
/*        <span class='zoom' id='ex1'><img src="{{ asset(['uploads/livres/',pub.getLivre().getImageL()]|join )}}" height="219" width="300" id='jack' alt=''/></span>*/
/*       </div>*/
/*                 <!-- Strat Book Image Section -->*/
/*                 */
/*                 <!-- Strat Book Overview Section -->    */
/*         <div class="span6">*/
/*       <strong class="title">{{pub.getLivre().getNomLivre()}}</strong>*/
/*        	<p>{{pub.getPresentation()}}</p>  */
/*         <strong class="title">Auteur :</strong>*/
/*         <p>{{pub.getLivre().getAuteur()}}</p> */
/*         <strong class="title">Langue :</strong>*/
/*         <p>{{pub.getLivre().getLangue()}}</p> */
/*          <strong class="title">Etat :</strong>*/
/*         <p>{{pub.getLivre().getEtat()}}</p>       */
/*          <strong class="title">Prix :</strong>*/
/*         <p>{{pub.getLivre().getPrix()}}</p> */
/*          <strong class="title">Nombre des pages :</strong>*/
/*         <p>{{pub.getLivre().getNbrPages()}}</p> */
/*          <strong class="title">Type publication :</strong>*/
/*         <p>{{pub.getType()}}</p> */
/*         <strong class="title">{{ socialButtons() }}*/
/*                  </strong>*/
/*         */
/*                       */
/*                    </div>*/
/*                 <!-- End Book Overview Section -->*/
/*                 </div>*/
/*       		  </section>       */
/* {% endblock %}*/
/*     */
/*    */
