<?php

/* SmartBookLecteurBundle:publication:deposer_annonce.html.twig */
class __TwigTemplate_46ade22370e2faf0aff31bd56aee2dda4ceb8251fe6992c1b1bc1e002324f5b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:publication:deposer_annonce.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_contenu($context, array $blocks = array())
    {
        echo " 
   
    <center> <h1 class=\"titre_annonce\">Annonce ";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
        echo "</h1></center>
    ";
        // line 7
        if (((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")) == "achat")) {
            // line 8
            echo "     <center><div id=\"form_pub\">
        <form action=\"\" method=\"post\">
      <table>
          <tr>
              <td>Titre :</td>
              <td><input type=\"text\" name=\"titre\"></td>            
          </tr>
          <tr>
              <td>Texte annonnce :</td>
              <td><textarea name=\"presentation\"/></textarea></td>            
          </tr>                 
          <tr>
              <td><button type=\"submit\" id=\"btn_form\">Publier</button></td>
              <td><button type=\"reset\" id=\"btn_form\">Annuler</button></td>            
          </tr>
      </table>     
  </form>
  </div>
    </center>
     ";
        } elseif ((        // line 27
(isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")) != "achat")) {
            // line 28
            echo "     
    <center><div id=\"form_pub\">
  <form action=\"\" method=\"post\">
      <table>
          <tr>
              <td>Titre :</td>
              <td><input type=\"text\" name=\"titre\"></td>            
          </tr>
          <tr>
              <td>Texte annonnce :</td>
              <td><textarea name=\"presentation\"/></textarea></td>            
          </tr>
              
          <tr>
              <td>Nom livre :</td>
              <td><input type=\"text\" name=\"noml\"></td>            
          </tr>
          <tr>
              <td>Auteur :</td>
              <td><input type=\"text\" name=\"auteur\"></td>            
          </tr>
          <tr>
              <td>Langue :</td>
              <td><select name=\"langue\">
                      <option value=\"Arabe\">Arabe</option>
                      <option value=\"Francais\">Français</option>
                      <option value=\"Anglais\">Anglais</option>
                  </select></td>            
          </tr>
          <tr>
              <td>Prix :</td>
              <td><input type=\"number\" name=\"prix\"></td>  
              <td> DT</td>
          </tr>
          <tr>
              <td>Nombre des pages </td>
              <td><input type=\"number\" name=\"nbrp\"></td>            
          </tr>
          <tr>
              <td>Date edition :</td>
              <td><input type=\"date\" name=\"datee\"></td>            
          </tr>
          <tr>
              <td>Etat :</td>
              <td><input type=\"text\" name=\"etat\"></td>            
          </tr>
          <tr>
              <td>Categorie</td>
              <td><select name=\"categorie\">
                      <option value=\"Politique\">Politique</option>
                      <option value=\"Sciences\">Sciences</option>
                      <option value=\"Art\">Art</option>
                      <option value=\"Theatre \">Theatre</option>
                      <option value=\"Sport\">Sport</option>
                      <option value=\"Cuisine\">Cuisine</option>
                  </select></td>            
          </tr>
          <tr>
              <td>Photo livre :</td>
              <td><input type=\"file\" name=\"image\"></td>            
          </tr>     
          <tr>
              <td><button type=\"submit\" id=\"btn_form\">Publier</button></td>
              <td><button type=\"reset\" id=\"btn_form\">Annuler</button></td>            
          </tr>
      </table>
      
  </form>
  </div>
    </center>
      ";
        }
        // line 99
        echo "    ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:publication:deposer_annonce.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 99,  63 => 28,  61 => 27,  40 => 8,  38 => 7,  34 => 6,  28 => 4,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* */
/* {% block contenu %} */
/*    */
/*     <center> <h1 class="titre_annonce">Annonce {{type}}</h1></center>*/
/*     {% if type == "achat" %}*/
/*      <center><div id="form_pub">*/
/*         <form action="" method="post">*/
/*       <table>*/
/*           <tr>*/
/*               <td>Titre :</td>*/
/*               <td><input type="text" name="titre"></td>            */
/*           </tr>*/
/*           <tr>*/
/*               <td>Texte annonnce :</td>*/
/*               <td><textarea name="presentation"/></textarea></td>            */
/*           </tr>                 */
/*           <tr>*/
/*               <td><button type="submit" id="btn_form">Publier</button></td>*/
/*               <td><button type="reset" id="btn_form">Annuler</button></td>            */
/*           </tr>*/
/*       </table>     */
/*   </form>*/
/*   </div>*/
/*     </center>*/
/*      {% elseif  type!="achat"%}*/
/*      */
/*     <center><div id="form_pub">*/
/*   <form action="" method="post">*/
/*       <table>*/
/*           <tr>*/
/*               <td>Titre :</td>*/
/*               <td><input type="text" name="titre"></td>            */
/*           </tr>*/
/*           <tr>*/
/*               <td>Texte annonnce :</td>*/
/*               <td><textarea name="presentation"/></textarea></td>            */
/*           </tr>*/
/*               */
/*           <tr>*/
/*               <td>Nom livre :</td>*/
/*               <td><input type="text" name="noml"></td>            */
/*           </tr>*/
/*           <tr>*/
/*               <td>Auteur :</td>*/
/*               <td><input type="text" name="auteur"></td>            */
/*           </tr>*/
/*           <tr>*/
/*               <td>Langue :</td>*/
/*               <td><select name="langue">*/
/*                       <option value="Arabe">Arabe</option>*/
/*                       <option value="Francais">Français</option>*/
/*                       <option value="Anglais">Anglais</option>*/
/*                   </select></td>            */
/*           </tr>*/
/*           <tr>*/
/*               <td>Prix :</td>*/
/*               <td><input type="number" name="prix"></td>  */
/*               <td> DT</td>*/
/*           </tr>*/
/*           <tr>*/
/*               <td>Nombre des pages </td>*/
/*               <td><input type="number" name="nbrp"></td>            */
/*           </tr>*/
/*           <tr>*/
/*               <td>Date edition :</td>*/
/*               <td><input type="date" name="datee"></td>            */
/*           </tr>*/
/*           <tr>*/
/*               <td>Etat :</td>*/
/*               <td><input type="text" name="etat"></td>            */
/*           </tr>*/
/*           <tr>*/
/*               <td>Categorie</td>*/
/*               <td><select name="categorie">*/
/*                       <option value="Politique">Politique</option>*/
/*                       <option value="Sciences">Sciences</option>*/
/*                       <option value="Art">Art</option>*/
/*                       <option value="Theatre ">Theatre</option>*/
/*                       <option value="Sport">Sport</option>*/
/*                       <option value="Cuisine">Cuisine</option>*/
/*                   </select></td>            */
/*           </tr>*/
/*           <tr>*/
/*               <td>Photo livre :</td>*/
/*               <td><input type="file" name="image"></td>            */
/*           </tr>     */
/*           <tr>*/
/*               <td><button type="submit" id="btn_form">Publier</button></td>*/
/*               <td><button type="reset" id="btn_form">Annuler</button></td>            */
/*           </tr>*/
/*       </table>*/
/*       */
/*   </form>*/
/*   </div>*/
/*     </center>*/
/*       {% endif %}*/
/*     {% endblock %}*/
/* */
