<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_100b91e0bdef5383221a08a646a045baffabfd87cf63118deda34624c9aea937 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout2.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_contenu($context, array $blocks = array())
    {
        // line 3
        echo "    <center>
";
        // line 4
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 7
        echo "    </center>
";
    }

    // line 4
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 5
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 5)->display($context);
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 5,  42 => 4,  37 => 7,  35 => 4,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout2.html.twig" %}*/
/* {% block contenu %}*/
/*     <center>*/
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/*     </center>*/
/* {% endblock %}*/
/* */
