<?php

/* SmartBookLecteurBundle:Evenement:liste_evenements.html.twig */
class __TwigTemplate_b9db07390e84f6e2a4b12fba686f5b9d73a7ee94287d42470d7ce7198d547328 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:Evenement:liste_evenements.html.twig", 3);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_contenu($context, array $blocks = array())
    {
        // line 5
        echo "    <section id=\"content-holder\" class=\"container-fluid container\">
    <section class=\"row-fluid\">
     
    \t<div class=\"heading-bar\">
        \t<h2>Evenements</h2>         
           </div>
          ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range((twig_length_filter($this->env, (isset($context["evenements"]) ? $context["evenements"] : $this->getContext($context, "evenements"))) - 1), 0));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 12
            echo "          <section class=\"list-holder\">
            \t<article class=\"item-holder\">
                \t<div class=\"span2\">
                    \t<a href=\"book-detail.html\"><img src=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(twig_join_filter(array(0 => "uploads/evenements/", 1 => $this->getAttribute($this->getAttribute((isset($context["evenements"]) ? $context["evenements"] : $this->getContext($context, "evenements")), $context["i"], array(), "array"), "getImage", array(), "method")))), "html", null, true);
            echo "\" alt=\"Image07\" /></a> </div>
                    <div class=\"span10\">
                    \t<div class=\"title-bar\"><a href=\"\">Titre evenement</a></div>
                        <strong>Titre evenements</strong>
                        <p>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["evenements"]) ? $context["evenements"] : $this->getContext($context, "evenements")), $context["i"], array(), "array"), "getTitre", array(), "method"), "html", null, true);
            echo "</p>
                        <strong>Lieu</strong>
                        <p>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["evenements"]) ? $context["evenements"] : $this->getContext($context, "evenements")), $context["i"], array(), "array"), "getLieuEve", array(), "method"), "html", null, true);
            echo "</p>          
                        <strong>Durée</strong>
                        <p>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["evenements"]) ? $context["evenements"] : $this->getContext($context, "evenements")), $context["i"], array(), "array"), "getDureeEve", array(), "method"), "html", null, true);
            echo " minutes</p>
                        <strong>Nombre des particiapant</strong>
                        <p>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["evenements"]) ? $context["evenements"] : $this->getContext($context, "evenements")), $context["i"], array(), "array"), "getNbrParticipant", array(), "method"), "html", null, true);
            echo "</p>
          
                        <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_participer", array("id_e" => $this->getAttribute($this->getAttribute((isset($context["evenements"]) ? $context["evenements"] : $this->getContext($context, "evenements")), $context["i"], array(), "array"), "getIdE", array(), "method"))), "html", null, true);
            echo "\" class=\"shop-btn\">Participer</a>
                        </div>
                    
                </article>
          </section>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "        </section>
        </section>
    ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Evenement:liste_evenements.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 33,  75 => 27,  70 => 25,  65 => 23,  60 => 21,  55 => 19,  48 => 15,  43 => 12,  39 => 11,  31 => 5,  28 => 4,  11 => 3,);
    }
}
/* {# empty Twig template #}*/
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* {% block contenu %}*/
/*     <section id="content-holder" class="container-fluid container">*/
/*     <section class="row-fluid">*/
/*      */
/*     	<div class="heading-bar">*/
/*         	<h2>Evenements</h2>         */
/*            </div>*/
/*           {% for i in evenements|length-1..0 %}*/
/*           <section class="list-holder">*/
/*             	<article class="item-holder">*/
/*                 	<div class="span2">*/
/*                     	<a href="book-detail.html"><img src="{{asset(['uploads/evenements/',evenements[i].getImage()]|join )}}" alt="Image07" /></a> </div>*/
/*                     <div class="span10">*/
/*                     	<div class="title-bar"><a href="">Titre evenement</a></div>*/
/*                         <strong>Titre evenements</strong>*/
/*                         <p>{{evenements[i].getTitre()}}</p>*/
/*                         <strong>Lieu</strong>*/
/*                         <p>{{evenements[i].getLieuEve()}}</p>          */
/*                         <strong>Durée</strong>*/
/*                         <p>{{evenements[i].getDureeEve()}} minutes</p>*/
/*                         <strong>Nombre des particiapant</strong>*/
/*                         <p>{{evenements[i].getNbrParticipant()}}</p>*/
/*           */
/*                         <a href="{{path('smart_book_lecteur_participer',{'id_e' : evenements[i].getIdE()})}}" class="shop-btn">Participer</a>*/
/*                         </div>*/
/*                     */
/*                 </article>*/
/*           </section>*/
/*           {% endfor %}*/
/*         </section>*/
/*         </section>*/
/*     {% endblock %}*/
/* */
/* */
