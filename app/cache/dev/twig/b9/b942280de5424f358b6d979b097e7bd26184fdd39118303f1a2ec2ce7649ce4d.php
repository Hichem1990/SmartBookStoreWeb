<?php

/* SmartBookLecteurBundle:lecteur:index.html.twig */
class __TwigTemplate_5bcbac78ce56f0cdfa3417ab7c689b3de9829b3ff1bc94825b7e5a708a3c9f6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout.html.twig", "SmartBookLecteurBundle:lecteur:index.html.twig", 2);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    
    <h1>Ici view de lecteur </h1>  
    ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:lecteur:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout.html.twig" %}*/
/* {% block content %}*/
/*     */
/*     <h1>Ici view de lecteur </h1>  */
/*     {% endblock %}*/
