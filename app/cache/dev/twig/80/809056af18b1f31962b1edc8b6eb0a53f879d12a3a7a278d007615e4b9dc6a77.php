<?php

/* SmartBookLecteurBundle::layout6.html.twig */
class __TwigTemplate_ac6d90286535fa0a4056f801536c57b5242cea0d0667abfca7520e8afeab06a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html>
<head>
<title>Book Store</title>
<!--[if lt IE 9]>
\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
<![endif]-->
<!--[if lt IE 9]>
\t<script src=\"http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js\"></script>
<![endif]-->
<meta http-equiv=\"cache-control\" content=\"no-cache\"></meta>
<meta charset=\"utf-8\"></meta>
<meta name=\"viewport\" content=\"initial-scale=1, maximum-scale=1\"></meta>
<meta name=\"viewport\" content=\"width=device-width\"></meta>
<!-- Css Files Start -->
<link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style_personnalisé.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- All css -->
<link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bs.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Bootstrap Css -->
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main-slider.css"), "html", null, true);
        echo "\" /><!-- Main Slider Css -->
<!--[if lte IE 10]><link rel=\"stylesheet\" type=\"text/css\" href=\"css/customIE.css\" /><![endif]-->
<link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.css\""), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Font Awesome Css -->
<link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome-ie7.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Font Awesome iE7 Css -->
<noscript>
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/noJS.css"), "html", null, true);
        echo "\" />
</noscript>
<!-- Css Files End -->
 <script
    src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAMjKGV-a0gQYCeYJCrdDT2JThpg9U4ydk
    &callback=initMap\">
    </script>
    <script type=\"text/javascript\">
    var map;
    function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 8
   });
    }
    </script>
</head>
<body>
     
<!-- Start Main Wrapper -->
<div class=\"wrapper\">
  <!-- Start Main Header -->
  <!-- Start Top Nav Bar -->
  <section class=\"top-nav-bar\">
    <section class=\"container-fluid container\">
      <section class=\"row-fluid\">
        <section class=\"span6\">
          <ul class=\"top-nav\">
            <li><a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("smart_book");
        echo "\">Gestion des Utilisateurs</a></li>
             <li><a href=\"";
        // line 53
        echo $this->env->getExtension('routing')->getPath("mart_book_lecteur_recherche");
        echo "\">Recherche des utilisateurs selon le nom</a></li>
              <li><a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("mart_book_lecteur_search");
        echo "\">Recherche des utilisateurs selon l'id</a></li>
               <li><a href=\"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("smart_book_reclam");
        echo "\">Reclamation administrative</a></li>
             </ul>
            </li>                    
          </ul>
        </section>
  <section class=\"span6 e-commerce-list\">
          <ul>
            <li>Bienvenue! <a href=\"\"></a></li> 
             <li><a href=\"";
        // line 63
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "\">se déconnecter</a></li>    
          </ul>
          <div class=\"c-btn\"> <a href=\"cart.html\" class=\"cart-btn\">Cart</a>
            <div class=\"btn-group\">
              <button data-toggle=\"dropdown\" class=\"btn btn-mini dropdown-toggle\">0 item(s) - \$0.00<span class=\"caret\"></span></button>
              <ul class=\"dropdown-menu\">
                <li><a href=\"#\">Action</a></li>
                <li><a href=\"#\">Another action</a></li>
                <li><a href=\"#\">Something else here</a></li>
              </ul>
            </div>
          </div>
        </section>
      </section>
    </section>
  </section>
  <header id=\"main-header\">
    <section class=\"container-fluid container\">
      <section class=\"row-fluid\">
        <section class=\"span4\">
          <h1 id=\"logo\"> <a href=\"index.html\"><img src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" /></a> </h1>
        </section>
     
      </section>
    </section>
        <!-- /.navbar-inner -->
      </div>
      <!-- /.navbar -->
    </nav>
    
    <!-- End Main Nav Bar -->
  </header>
  ";
        // line 95
        $this->displayBlock('contenu', $context, $blocks);
        // line 98
        echo "   ";
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 100
        echo "
 
   
  <!-- End Footer Top 2 -->
  <!-- Start Main Footer -->
  <footer id=\"main-footer\">
    <section class=\"social-ico-bar\">
      <section class=\"container\">
        <section class=\"row-fluid\">
          <article class=\"span6\">
            <p>© 2013  BookShoppe’ - Premium WooCommerce Theme. </p>
          </article>
          <article class=\"span6 copy-right\">
            <p>Designed by <a href=\"http://www.crunchpress.com/\">Crunchpress.com</a></p>
          </article>
        </section>
      </section>
    </section>
  </footer>
<!-- End Main Wrapper -->
<!-- JS Files Start -->
<script type=\"text/javascript\" src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/lib.js"), "html", null, true);
        echo "\"></script><!-- lib Js -->
<script type=\"text/javascript\" src=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr.js"), "html", null, true);
        echo "\"></script><!-- Modernizr -->
<script type=\"text/javascript\" src=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/easing.js"), "html", null, true);
        echo "\"></script><!-- Easing js -->
<script type=\"text/javascript\" src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bs.js"), "html", null, true);
        echo "\"></script><!-- Bootstrap -->
<script type=\"text/javascript\" src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bxslider.js"), "html", null, true);
        echo "\"></script><!-- BX Slider -->
<script type=\"text/javascript\" src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/input-clear.js"), "html", null, true);
        echo "\"></script><!-- Input Clear -->
<script src=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/range-slider.js"), "html", null, true);
        echo "\"></script><!-- Range Slider -->
<script src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.zoom.js"), "html", null, true);
        echo "\"></script><!-- Zoom Effect -->
<script type=\"text/javascript\" src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bookblock.js"), "html", null, true);
        echo "\"></script><!-- Flip Slider -->
<script type=\"text/javascript\" src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/custom.js"), "html", null, true);
        echo "\"></script><!-- Custom js -->
<script type=\"text/javascript\" src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/social.js"), "html", null, true);
        echo "\"></script><!-- Social Icons -->
<!-- JS Files End -->
<noscript>
<style>
\t#socialicons>a span { top: 0px; left: -100%; -webkit-transition: all 0.3s ease; -moz-transition: all 0.3s ease-in-out; -o-transition: all 0.3s ease-in-out; -ms-transition: all 0.3s ease-in-out; transition: all 0.3s \tease-in-out;}
\t#socialicons>ahover div{left: 0px;}
\t</style>
</noscript>
<script type=\"text/javascript\">
  /* <![CDATA[ */
  \$(document).ready(function() {
  \$('.social_active').hoverdir( {} );
})
/* ]]> */
</script>
</body>
</html>
";
    }

    // line 95
    public function block_contenu($context, array $blocks = array())
    {
        // line 96
        echo "
  ";
    }

    // line 98
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 99
        echo "       ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle::layout6.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 99,  254 => 98,  249 => 96,  246 => 95,  224 => 131,  220 => 130,  216 => 129,  212 => 128,  208 => 127,  204 => 126,  200 => 125,  196 => 124,  192 => 123,  188 => 122,  184 => 121,  161 => 100,  158 => 98,  156 => 95,  141 => 83,  118 => 63,  107 => 55,  103 => 54,  99 => 53,  95 => 52,  64 => 24,  59 => 22,  55 => 21,  50 => 19,  46 => 18,  42 => 17,  38 => 16,  21 => 1,);
    }
}
/* <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">*/
/* <html>*/
/* <head>*/
/* <title>Book Store</title>*/
/* <!--[if lt IE 9]>*/
/* 	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>*/
/* <![endif]-->*/
/* <!--[if lt IE 9]>*/
/* 	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>*/
/* <![endif]-->*/
/* <meta http-equiv="cache-control" content="no-cache"></meta>*/
/* <meta charset="utf-8"></meta>*/
/* <meta name="viewport" content="initial-scale=1, maximum-scale=1"></meta>*/
/* <meta name="viewport" content="width=device-width"></meta>*/
/* <!-- Css Files Start -->*/
/* <link href="{{asset('css/style_personnalisé.css')}}" rel="stylesheet">*/
/* <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" /><!-- All css -->*/
/* <link href="{{asset('css/bs.css')}}" rel="stylesheet" type="text/css" /><!-- Bootstrap Css -->*/
/* <link rel="stylesheet" type="text/css" href="{{asset('css/main-slider.css')}}" /><!-- Main Slider Css -->*/
/* <!--[if lte IE 10]><link rel="stylesheet" type="text/css" href="css/customIE.css" /><![endif]-->*/
/* <link href="{{asset('css/font-awesome.css"')}}" rel="stylesheet" type="text/css" /><!-- Font Awesome Css -->*/
/* <link href="{{asset('css/font-awesome-ie7.css')}}" rel="stylesheet" type="text/css" /><!-- Font Awesome iE7 Css -->*/
/* <noscript>*/
/* <link rel="stylesheet" type="text/css" href="{{asset('css/noJS.css')}}" />*/
/* </noscript>*/
/* <!-- Css Files End -->*/
/*  <script*/
/*     src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMjKGV-a0gQYCeYJCrdDT2JThpg9U4ydk*/
/*     &callback=initMap">*/
/*     </script>*/
/*     <script type="text/javascript">*/
/*     var map;*/
/*     function initMap() {*/
/*     map = new google.maps.Map(document.getElementById('map'), {*/
/*     center: {lat: -34.397, lng: 150.644},*/
/*     zoom: 8*/
/*    });*/
/*     }*/
/*     </script>*/
/* </head>*/
/* <body>*/
/*      */
/* <!-- Start Main Wrapper -->*/
/* <div class="wrapper">*/
/*   <!-- Start Main Header -->*/
/*   <!-- Start Top Nav Bar -->*/
/*   <section class="top-nav-bar">*/
/*     <section class="container-fluid container">*/
/*       <section class="row-fluid">*/
/*         <section class="span6">*/
/*           <ul class="top-nav">*/
/*             <li><a href="{{path ('smart_book') }}">Gestion des Utilisateurs</a></li>*/
/*              <li><a href="{{path ('mart_book_lecteur_recherche') }}">Recherche des utilisateurs selon le nom</a></li>*/
/*               <li><a href="{{path ('mart_book_lecteur_search') }}">Recherche des utilisateurs selon l'id</a></li>*/
/*                <li><a href="{{path ('smart_book_reclam') }}">Reclamation administrative</a></li>*/
/*              </ul>*/
/*             </li>                    */
/*           </ul>*/
/*         </section>*/
/*   <section class="span6 e-commerce-list">*/
/*           <ul>*/
/*             <li>Bienvenue! <a href=""></a></li> */
/*              <li><a href="{{ path('fos_user_security_logout')}}">se déconnecter</a></li>    */
/*           </ul>*/
/*           <div class="c-btn"> <a href="cart.html" class="cart-btn">Cart</a>*/
/*             <div class="btn-group">*/
/*               <button data-toggle="dropdown" class="btn btn-mini dropdown-toggle">0 item(s) - $0.00<span class="caret"></span></button>*/
/*               <ul class="dropdown-menu">*/
/*                 <li><a href="#">Action</a></li>*/
/*                 <li><a href="#">Another action</a></li>*/
/*                 <li><a href="#">Something else here</a></li>*/
/*               </ul>*/
/*             </div>*/
/*           </div>*/
/*         </section>*/
/*       </section>*/
/*     </section>*/
/*   </section>*/
/*   <header id="main-header">*/
/*     <section class="container-fluid container">*/
/*       <section class="row-fluid">*/
/*         <section class="span4">*/
/*           <h1 id="logo"> <a href="index.html"><img src="{{asset('images/logo.png')}}" /></a> </h1>*/
/*         </section>*/
/*      */
/*       </section>*/
/*     </section>*/
/*         <!-- /.navbar-inner -->*/
/*       </div>*/
/*       <!-- /.navbar -->*/
/*     </nav>*/
/*     */
/*     <!-- End Main Nav Bar -->*/
/*   </header>*/
/*   {% block contenu %}*/
/* */
/*   {% endblock %}*/
/*    {% block fos_user_content %}*/
/*        {% endblock fos_user_content %}*/
/* */
/*  */
/*    */
/*   <!-- End Footer Top 2 -->*/
/*   <!-- Start Main Footer -->*/
/*   <footer id="main-footer">*/
/*     <section class="social-ico-bar">*/
/*       <section class="container">*/
/*         <section class="row-fluid">*/
/*           <article class="span6">*/
/*             <p>© 2013  BookShoppe’ - Premium WooCommerce Theme. </p>*/
/*           </article>*/
/*           <article class="span6 copy-right">*/
/*             <p>Designed by <a href="http://www.crunchpress.com/">Crunchpress.com</a></p>*/
/*           </article>*/
/*         </section>*/
/*       </section>*/
/*     </section>*/
/*   </footer>*/
/* <!-- End Main Wrapper -->*/
/* <!-- JS Files Start -->*/
/* <script type="text/javascript" src="{{asset('js/lib.js')}}"></script><!-- lib Js -->*/
/* <script type="text/javascript" src="{{asset('js/modernizr.js')}}"></script><!-- Modernizr -->*/
/* <script type="text/javascript" src="{{asset('js/easing.js')}}"></script><!-- Easing js -->*/
/* <script type="text/javascript" src="{{asset('js/bs.js')}}"></script><!-- Bootstrap -->*/
/* <script type="text/javascript" src="{{asset('js/bxslider.js')}}"></script><!-- BX Slider -->*/
/* <script type="text/javascript" src="{{asset('js/input-clear.js')}}"></script><!-- Input Clear -->*/
/* <script src="{{asset('js/range-slider.js')}}"></script><!-- Range Slider -->*/
/* <script src="{{asset('js/jquery.zoom.js')}}"></script><!-- Zoom Effect -->*/
/* <script type="text/javascript" src="{{asset('js/bookblock.js')}}"></script><!-- Flip Slider -->*/
/* <script type="text/javascript" src="{{asset('js/custom.js')}}"></script><!-- Custom js -->*/
/* <script type="text/javascript" src="{{asset('js/social.js')}}"></script><!-- Social Icons -->*/
/* <!-- JS Files End -->*/
/* <noscript>*/
/* <style>*/
/* 	#socialicons>a span { top: 0px; left: -100%; -webkit-transition: all 0.3s ease; -moz-transition: all 0.3s ease-in-out; -o-transition: all 0.3s ease-in-out; -ms-transition: all 0.3s ease-in-out; transition: all 0.3s 	ease-in-out;}*/
/* 	#socialicons>ahover div{left: 0px;}*/
/* 	</style>*/
/* </noscript>*/
/* <script type="text/javascript">*/
/*   /* <![CDATA[ *//* */
/*   $(document).ready(function() {*/
/*   $('.social_active').hoverdir( {} );*/
/* })*/
/* /* ]]> *//* */
/* </script>*/
/* </body>*/
/* </html>*/
/* */
