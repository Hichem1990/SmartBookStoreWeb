<?php

/* SmartBookLecteurBundle:Livre:compte_libraire.html.twig */
class __TwigTemplate_1c24e515f9992393ad39b81402bbfe3885a6d6d8808798c63572caf2c882f209 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html>
<head>
<title>Book Store</title>
<!--[if lt IE 9]>
\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
<![endif]-->
<!--[if lt IE 9]>
\t<script src=\"http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js\"></script>
<![endif]-->
<meta http-equiv=\"cache-control\" content=\"no-cache\"></meta>
<meta charset=\"utf-8\"></meta>
<meta name=\"viewport\" content=\"initial-scale=1, maximum-scale=1\"></meta>
<meta name=\"viewport\" content=\"width=device-width\"></meta>
<!-- Css Files Start -->
<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style_personnalisé.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- All css -->
<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bs.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Bootstrap Css -->
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/main-slider.css"), "html", null, true);
        echo "\" /><!-- Main Slider Css -->
<!--[if lte IE 10]><link rel=\"stylesheet\" type=\"text/css\" href=\"css/customIE.css\" /><![endif]-->
<link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.css\""), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Font Awesome Css -->
<link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome-ie7.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" /><!-- Font Awesome iE7 Css -->
<noscript>
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/noJS.css"), "html", null, true);
        echo "\" />
</noscript>
<!-- Css Files End -->
 
</head>
<body>
<!-- Start Main Wrapper -->
<div class=\"wrapper\">
  <!-- Start Main Header -->
  <!-- Start Top Nav Bar -->
  <section class=\"top-nav-bar\">
    <section class=\"container-fluid container\">
      <section class=\"row-fluid\">
        <section class=\"span6\">
          <ul class=\"top-nav\">
           
            <li><a href=\"";
        // line 41
        echo $this->env->getExtension('routing')->getPath("smart_book_livre_Ajout");
        echo "\">Ajouter Livre</a></li>
             <li><a href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("smart_book_livre_affiche");
        echo "\">Gerer mes livres</a></li>
             <li><a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("Smart_book_livre_Rechercher");
        echo "\">Rechercher livres</a></li>
             <li><a href=\"";
        // line 44
        echo $this->env->getExtension('routing')->getPath("smart_book_lecteur_pie");
        echo "\">Afficher statistiques</a></li>  
          </ul>
        </section>
        <section class=\"span6 e-commerce-list\">
          <ul>
            <li>Bienvenue! <a href=\"\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        echo "</a></li> 
             <li><a href=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "\">se déconnecter</a></li>    
          </ul>
          <div class=\"c-btn\"> <a href=\"cart.html\" class=\"cart-btn\">Cart</a>
            <div class=\"btn-group\">
              <button data-toggle=\"dropdown\" class=\"btn btn-mini dropdown-toggle\">0 item(s) - \$0.00<span class=\"caret\"></span></button>
              <ul class=\"dropdown-menu\">
                <li><a href=\"#\">Action</a></li>
                <li><a href=\"#\">Another action</a></li>
                <li><a href=\"#\">Something else here</a></li>
              </ul>
            </div>
          </div>
        </section>
      </section>
    </section>
  </section>
  <!-- End Top Nav Bar -->
  <header id=\"main-header\">
    <section class=\"container-fluid container\">
      <section class=\"row-fluid\">
        <section class=\"span4\">
          <h1 id=\"logo\"> <a href=\"index.html\"><img src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" /></a> </h1>
        </section>
     
      </section>
    </section>
    <!-- Start Main Nav Bar -->
    <nav id=\"nav\">
      <div class=\"navbar navbar-inverse\">
        <div class=\"navbar-inner\">
          <button type=\"button\" class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\"> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span> </button>
          <div class=\"nav-collapse collapse\">           
          </div>
          <!--/.nav-collapse -->
        </div>
        <!-- /.navbar-inner -->
      </div>
      <!-- /.navbar -->
    </nav>
    <!-- End Main Nav Bar -->
  </header>
  ";
        // line 91
        $this->displayBlock('contenu', $context, $blocks);
        // line 94
        echo "   
 
   
  <!-- End Footer Top 2 -->
  <!-- Start Main Footer -->
  <footer id=\"main-footer\">
    <section class=\"social-ico-bar\">
      <section class=\"container\">
        <section class=\"row-fluid\">
          <article class=\"span6\">
            <p>© Le 352 Esprit 2015 </p>
          </article>
          <article class=\"span6 copy-right\">
            <p>Designed by <a href=\"\">Le352.com</a></p>
          </article>
        </section>
      </section>
    </section>
  </footer>
<!-- End Main Wrapper -->
<!-- JS Files Start -->
<script type=\"text/javascript\" src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/lib.js"), "html", null, true);
        echo "\"></script><!-- lib Js -->
<script type=\"text/javascript\" src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/modernizr.js"), "html", null, true);
        echo "\"></script><!-- Modernizr -->
<script type=\"text/javascript\" src=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/easing.js"), "html", null, true);
        echo "\"></script><!-- Easing js -->
<script type=\"text/javascript\" src=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bs.js"), "html", null, true);
        echo "\"></script><!-- Bootstrap -->
<script type=\"text/javascript\" src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bxslider.js"), "html", null, true);
        echo "\"></script><!-- BX Slider -->
<script type=\"text/javascript\" src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/input-clear.js"), "html", null, true);
        echo "\"></script><!-- Input Clear -->
<script src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/range-slider.js"), "html", null, true);
        echo "\"></script><!-- Range Slider -->
<script src=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.zoom.js"), "html", null, true);
        echo "\"></script><!-- Zoom Effect -->
<script type=\"text/javascript\" src=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bookblock.js"), "html", null, true);
        echo "\"></script><!-- Flip Slider -->
<script type=\"text/javascript\" src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/custom.js"), "html", null, true);
        echo "\"></script><!-- Custom js -->
<script type=\"text/javascript\" src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/social.js"), "html", null, true);
        echo "\"></script><!-- Social Icons -->
<!-- JS Files End -->
<noscript>
<style>
\t#socialicons>a span { top: 0px; left: -100%; -webkit-transition: all 0.3s ease; -moz-transition: all 0.3s ease-in-out; -o-transition: all 0.3s ease-in-out; -ms-transition: all 0.3s ease-in-out; transition: all 0.3s \tease-in-out;}
\t#socialicons>ahover div{left: 0px;}
\t</style>
</noscript>
<script type=\"text/javascript\">
  /* <![CDATA[ */
  \$(document).ready(function() {
  \$('.social_active').hoverdir( {} );
})
/* ]]> */
</script>
</body>
</html>
";
    }

    // line 91
    public function block_contenu($context, array $blocks = array())
    {
        // line 92
        echo "
  ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Livre:compte_libraire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  243 => 92,  240 => 91,  218 => 125,  214 => 124,  210 => 123,  206 => 122,  202 => 121,  198 => 120,  194 => 119,  190 => 118,  186 => 117,  182 => 116,  178 => 115,  155 => 94,  153 => 91,  130 => 71,  106 => 50,  102 => 49,  94 => 44,  90 => 43,  86 => 42,  82 => 41,  63 => 25,  58 => 23,  54 => 22,  49 => 20,  45 => 19,  41 => 18,  37 => 17,  20 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">*/
/* <html>*/
/* <head>*/
/* <title>Book Store</title>*/
/* <!--[if lt IE 9]>*/
/* 	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>*/
/* <![endif]-->*/
/* <!--[if lt IE 9]>*/
/* 	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>*/
/* <![endif]-->*/
/* <meta http-equiv="cache-control" content="no-cache"></meta>*/
/* <meta charset="utf-8"></meta>*/
/* <meta name="viewport" content="initial-scale=1, maximum-scale=1"></meta>*/
/* <meta name="viewport" content="width=device-width"></meta>*/
/* <!-- Css Files Start -->*/
/* <link href="{{asset('css/style_personnalisé.css')}}" rel="stylesheet">*/
/* <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" /><!-- All css -->*/
/* <link href="{{asset('css/bs.css')}}" rel="stylesheet" type="text/css" /><!-- Bootstrap Css -->*/
/* <link rel="stylesheet" type="text/css" href="{{asset('css/main-slider.css')}}" /><!-- Main Slider Css -->*/
/* <!--[if lte IE 10]><link rel="stylesheet" type="text/css" href="css/customIE.css" /><![endif]-->*/
/* <link href="{{asset('css/font-awesome.css"')}}" rel="stylesheet" type="text/css" /><!-- Font Awesome Css -->*/
/* <link href="{{asset('css/font-awesome-ie7.css')}}" rel="stylesheet" type="text/css" /><!-- Font Awesome iE7 Css -->*/
/* <noscript>*/
/* <link rel="stylesheet" type="text/css" href="{{asset('css/noJS.css')}}" />*/
/* </noscript>*/
/* <!-- Css Files End -->*/
/*  */
/* </head>*/
/* <body>*/
/* <!-- Start Main Wrapper -->*/
/* <div class="wrapper">*/
/*   <!-- Start Main Header -->*/
/*   <!-- Start Top Nav Bar -->*/
/*   <section class="top-nav-bar">*/
/*     <section class="container-fluid container">*/
/*       <section class="row-fluid">*/
/*         <section class="span6">*/
/*           <ul class="top-nav">*/
/*            */
/*             <li><a href="{{path("smart_book_livre_Ajout")}}">Ajouter Livre</a></li>*/
/*              <li><a href="{{path("smart_book_livre_affiche")}}">Gerer mes livres</a></li>*/
/*              <li><a href="{{path("Smart_book_livre_Rechercher")}}">Rechercher livres</a></li>*/
/*              <li><a href="{{path("smart_book_lecteur_pie")}}">Afficher statistiques</a></li>  */
/*           </ul>*/
/*         </section>*/
/*         <section class="span6 e-commerce-list">*/
/*           <ul>*/
/*             <li>Bienvenue! <a href="">{{ app.user.username }}</a></li> */
/*              <li><a href="{{ path('fos_user_security_logout')}}">se déconnecter</a></li>    */
/*           </ul>*/
/*           <div class="c-btn"> <a href="cart.html" class="cart-btn">Cart</a>*/
/*             <div class="btn-group">*/
/*               <button data-toggle="dropdown" class="btn btn-mini dropdown-toggle">0 item(s) - $0.00<span class="caret"></span></button>*/
/*               <ul class="dropdown-menu">*/
/*                 <li><a href="#">Action</a></li>*/
/*                 <li><a href="#">Another action</a></li>*/
/*                 <li><a href="#">Something else here</a></li>*/
/*               </ul>*/
/*             </div>*/
/*           </div>*/
/*         </section>*/
/*       </section>*/
/*     </section>*/
/*   </section>*/
/*   <!-- End Top Nav Bar -->*/
/*   <header id="main-header">*/
/*     <section class="container-fluid container">*/
/*       <section class="row-fluid">*/
/*         <section class="span4">*/
/*           <h1 id="logo"> <a href="index.html"><img src="{{asset('images/logo.png')}}" /></a> </h1>*/
/*         </section>*/
/*      */
/*       </section>*/
/*     </section>*/
/*     <!-- Start Main Nav Bar -->*/
/*     <nav id="nav">*/
/*       <div class="navbar navbar-inverse">*/
/*         <div class="navbar-inner">*/
/*           <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>*/
/*           <div class="nav-collapse collapse">           */
/*           </div>*/
/*           <!--/.nav-collapse -->*/
/*         </div>*/
/*         <!-- /.navbar-inner -->*/
/*       </div>*/
/*       <!-- /.navbar -->*/
/*     </nav>*/
/*     <!-- End Main Nav Bar -->*/
/*   </header>*/
/*   {% block contenu %}*/
/* */
/*   {% endblock %}*/
/*    */
/*  */
/*    */
/*   <!-- End Footer Top 2 -->*/
/*   <!-- Start Main Footer -->*/
/*   <footer id="main-footer">*/
/*     <section class="social-ico-bar">*/
/*       <section class="container">*/
/*         <section class="row-fluid">*/
/*           <article class="span6">*/
/*             <p>© Le 352 Esprit 2015 </p>*/
/*           </article>*/
/*           <article class="span6 copy-right">*/
/*             <p>Designed by <a href="">Le352.com</a></p>*/
/*           </article>*/
/*         </section>*/
/*       </section>*/
/*     </section>*/
/*   </footer>*/
/* <!-- End Main Wrapper -->*/
/* <!-- JS Files Start -->*/
/* <script type="text/javascript" src="{{asset('js/lib.js')}}"></script><!-- lib Js -->*/
/* <script type="text/javascript" src="{{asset('js/modernizr.js')}}"></script><!-- Modernizr -->*/
/* <script type="text/javascript" src="{{asset('js/easing.js')}}"></script><!-- Easing js -->*/
/* <script type="text/javascript" src="{{asset('js/bs.js')}}"></script><!-- Bootstrap -->*/
/* <script type="text/javascript" src="{{asset('js/bxslider.js')}}"></script><!-- BX Slider -->*/
/* <script type="text/javascript" src="{{asset('js/input-clear.js')}}"></script><!-- Input Clear -->*/
/* <script src="{{asset('js/range-slider.js')}}"></script><!-- Range Slider -->*/
/* <script src="{{asset('js/jquery.zoom.js')}}"></script><!-- Zoom Effect -->*/
/* <script type="text/javascript" src="{{asset('js/bookblock.js')}}"></script><!-- Flip Slider -->*/
/* <script type="text/javascript" src="{{asset('js/custom.js')}}"></script><!-- Custom js -->*/
/* <script type="text/javascript" src="{{asset('js/social.js')}}"></script><!-- Social Icons -->*/
/* <!-- JS Files End -->*/
/* <noscript>*/
/* <style>*/
/* 	#socialicons>a span { top: 0px; left: -100%; -webkit-transition: all 0.3s ease; -moz-transition: all 0.3s ease-in-out; -o-transition: all 0.3s ease-in-out; -ms-transition: all 0.3s ease-in-out; transition: all 0.3s 	ease-in-out;}*/
/* 	#socialicons>ahover div{left: 0px;}*/
/* 	</style>*/
/* </noscript>*/
/* <script type="text/javascript">*/
/*   /* <![CDATA[ *//* */
/*   $(document).ready(function() {*/
/*   $('.social_active').hoverdir( {} );*/
/* })*/
/* /* ]]> *//* */
/* </script>*/
/* </body>*/
/* </html>*/
/* */
