<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_fdcad2fb4fe99344010e40fd12e927dc53c44de1e83a0d07dd68803a6f3f615b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout2.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_contenu($context, array $blocks = array())
    {
        // line 5
        $this->displayBlock('fos_user_content', $context, $blocks);
    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 6
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 7
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 9
        echo "<center>
<form action=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
  
                
        <center><h1>Authentification</h1></center>
    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />

    <label for=\"username\">Login :</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />

    <label for=\"password\">Mot passe :</label>
    <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" />
    </br>
    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
    <label for=\"remember_me\">Me souvenir</label>

    <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"Connexion\" />
     
    <p class=\"change_link\"> Pas encore membre ?</p>
      <div class=\"header_top_right\">
         <ul>                                                                                                                                                                                                                            
             ";
        // line 30
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 31
            echo "                           
                  ";
            // line 32
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 33
                echo "                  <li> <a href=\"";
                echo $this->env->getExtension('routing')->getPath("espritadmin_accueil");
                echo "\"> Admin </a>  </li>                  
                    ";
            }
            // line 34
            echo "                
                  ";
            // line 35
            if ($this->env->getExtension('security')->isGranted("ROLE_ROLE_LIBRAIRE")) {
                // line 36
                echo "                  <li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("smart_book_lecteur_Librairie");
                echo "\"> Libraire </a></li>
                    ";
            }
            // line 38
            echo "                    ";
            if ($this->env->getExtension('security')->isGranted("ROLE_LECTEUR")) {
                // line 39
                echo "                    <li>  <a href=\"";
                echo $this->env->getExtension('routing')->getPath("smart_book_lecteur_Affichage");
                echo "\"> Lecteur </a></li>
                       ";
            }
            // line 41
            echo "                ";
        } else {
            echo "                
                <li> <b><a href=\"";
            // line 42
            echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
            echo "\" >s'inscrire</a></b></li>                  
                ";
        }
        // line 43
        echo "                                    
        </ul>                                                               
      </form></cetner>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 43,  116 => 42,  111 => 41,  105 => 39,  102 => 38,  96 => 36,  94 => 35,  91 => 34,  85 => 33,  83 => 32,  80 => 31,  78 => 30,  62 => 17,  56 => 14,  49 => 10,  46 => 9,  40 => 7,  38 => 6,  32 => 5,  29 => 4,  11 => 1,);
    }
}
/*   {% extends "FOSUserBundle::layout2.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {%block contenu %}*/
/* {% block fos_user_content %}*/
/* {% if error %}*/
/*     <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/* {% endif %}*/
/* <center>*/
/* <form action="{{ path("fos_user_security_check") }}" method="post">*/
/*   */
/*                 */
/*         <center><h1>Authentification</h1></center>*/
/*     <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/* */
/*     <label for="username">Login :</label>*/
/*     <input type="text" id="username" name="_username" value="{{ last_username }}" required="required" />*/
/* */
/*     <label for="password">Mot passe :</label>*/
/*     <input type="password" id="password" name="_password" required="required" />*/
/*     </br>*/
/*     <input type="checkbox" id="remember_me" name="_remember_me" value="on" />*/
/*     <label for="remember_me">Me souvenir</label>*/
/* */
/*     <input type="submit" id="_submit" name="_submit" value="Connexion" />*/
/*      */
/*     <p class="change_link"> Pas encore membre ?</p>*/
/*       <div class="header_top_right">*/
/*          <ul>                                                                                                                                                                                                                            */
/*              {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                            */
/*                   {% if is_granted('ROLE_ADMIN') %}*/
/*                   <li> <a href="{{ path('espritadmin_accueil')}}"> Admin </a>  </li>                  */
/*                     {% endif %}                */
/*                   {% if is_granted('ROLE_ROLE_LIBRAIRE') %}*/
/*                   <li><a href="{{ path('smart_book_lecteur_Librairie')}}"> Libraire </a></li>*/
/*                     {% endif %}*/
/*                     {% if is_granted('ROLE_LECTEUR') %}*/
/*                     <li>  <a href="{{ path('smart_book_lecteur_Affichage')}}"> Lecteur </a></li>*/
/*                        {% endif %}*/
/*                 {% else %}                */
/*                 <li> <b><a href="{{ path('fos_user_registration_register') }}" >s'inscrire</a></b></li>                  */
/*                 {% endif %}                                    */
/*         </ul>                                                               */
/*       </form></cetner>*/
/* {% endblock fos_user_content %}*/
/* {%endblock %}*/
/* */
