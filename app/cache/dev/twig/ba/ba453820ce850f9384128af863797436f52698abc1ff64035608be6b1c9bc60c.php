<?php

/* SmartBookLecteurBundle:publication:liste_publications.html.twig */
class __TwigTemplate_d9ea2030b7ed2d9a01388d67ae42214aa7082fc368280bce284f897c1cba8ae3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:publication:liste_publications.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo " <section id=\"content-holder\" class=\"container-fluid container\">
    <section class=\"row-fluid\">
      <section class=\"span12 slider\">
        <section class=\"main-slider\">
          <div class=\"bb-custom-wrapper\">
            <div id=\"bb-bookblock\" class=\"bb-bookblock\">
                  ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range((twig_length_filter($this->env, (isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications"))) - 1), 0));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 11
            echo "                <div class=\"bb-item\">            
                <div class=\"bb-custom-content\">
                  <div class=\"slide-inner\">
                    <div class=\"span4 book-holder\"> <a href=\"book-detail.html\"><img src=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(twig_join_filter(array(0 => "uploads/livres/", 1 => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getLivre", array(), "method"), "getImageL", array(), "method")))), "html", null, true);
            echo "\" alt=\"Book\" /></a>
                      <div class=\"cart-price\"> <a class=\"cart-btn2\" href=\"cart.html\">Add to Cart</a> <span class=\"price\">";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getLivre", array(), "method"), "getPrix", array(), "method"), "html", null, true);
            echo " DT</span> </div>
                    </div>
                     
                    <div class=\"span8 book-detail\">
                      <h2>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getTitre", array(), "method"), "html", null, true);
            echo "</h2>
                      <strong class=\"title\">";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getLivre", array(), "method"), "getNomLivre", array(), "method"), "html", null, true);
            echo "</strong> <span class=\"rating-bar\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/raing-star2.png"), "html", null, true);
            echo "\" alt=\"Rating Star\" /> </span> 
                        <a href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_DetailPub", array("id" => $this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getIdP", array(), "method"))), "html", null, true);
            echo "\" class=\"shop-btn\">Plus information</a> 
                      <div class=\"cap-holder\">
                        <p><img src=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/image27.png"), "html", null, true);
            echo "\" alt=\"Best Choice\" align=\"right\"/>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getPresentation", array(), "method"), "html", null, true);
            echo "</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
             ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "                          
            </div>
          </div>
          <nav class=\"bb-custom-nav\"> <a href=\"#\" id=\"bb-nav-prev\" class=\"left-arrow\">Previous</a> <a href=\"#\" id=\"bb-nav-next\" class=\"right-arrow\">Next</a> </nav>
        </section>
        <span class=\"slider-bottom\"><img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/slider-bg.png"), "html", null, true);
        echo "\" alt=\"Shadow\"/></span> </section>
      <section class=\"span12 wellcome-msg m-bottom first\">
        <h2>Bienvenue chez SmartBook store.</h2>
        <p>Plateforme pour partage nos lectures et nos ecritures</p>
      </section>
    </section>
   
    
    <section class=\"row-fluid features-books\">
      <section class=\"span12 m-bottom\">
        <div class=\"heading-bar\">
          <h2>Autres publications</h2>
          <span class=\"h-line\"></span> </div>
          <center> <div class=\"slider1\">
           ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications"))) - 1)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 50
            echo "          <div class=\"slide\"> <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_DetailPub", array("id" => $this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getIdP", array(), "method"))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(twig_join_filter(array(0 => "uploads/livres/", 1 => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getLivre", array(), "method"), "getImageL", array(), "method")))), "html", null, true);
            echo "\" alt=\"\" class=\"pro-img\"/>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getLivre", array(), "method"), "getNomLivre", array(), "method"), "html", null, true);
            echo "</a> <span class=\"title\"><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_DetailPub", array("id" => $this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getIdP", array(), "method"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getLivre", array(), "method"), "getAuteur", array(), "method"), "html", null, true);
            echo "</a></span> <span class=\"rating-bar\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/rating-star.png"), "html", null, true);
            echo "\" alt=\"Rating Star\"/></span>
            <div class=\"cart-price\"> <a class=\"cart-btn2\" href=\"\">Add to Cart</a> <span class=\"price\">";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")), $context["i"], array(), "array"), "getLivre", array(), "method"), "getPrix", array(), "method"), "html", null, true);
            echo " DT</span> </div>
          </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "         
        </div>
        </center>
      </section>
    </section>
 </section>
";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:publication:liste_publications.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 53,  132 => 51,  117 => 50,  113 => 49,  96 => 35,  89 => 30,  74 => 23,  69 => 21,  63 => 20,  59 => 19,  52 => 15,  48 => 14,  43 => 11,  39 => 10,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* {% block contenu %}*/
/*  <section id="content-holder" class="container-fluid container">*/
/*     <section class="row-fluid">*/
/*       <section class="span12 slider">*/
/*         <section class="main-slider">*/
/*           <div class="bb-custom-wrapper">*/
/*             <div id="bb-bookblock" class="bb-bookblock">*/
/*                   {% for i in publications|length-1..0 %}*/
/*                 <div class="bb-item">            */
/*                 <div class="bb-custom-content">*/
/*                   <div class="slide-inner">*/
/*                     <div class="span4 book-holder"> <a href="book-detail.html"><img src="{{asset(['uploads/livres/',publications[i].getLivre().getImageL()]|join )}}" alt="Book" /></a>*/
/*                       <div class="cart-price"> <a class="cart-btn2" href="cart.html">Add to Cart</a> <span class="price">{{publications[i].getLivre().getPrix()}} DT</span> </div>*/
/*                     </div>*/
/*                      */
/*                     <div class="span8 book-detail">*/
/*                       <h2>{{publications[i].getTitre()}}</h2>*/
/*                       <strong class="title">{{publications[i].getLivre().getNomLivre()}}</strong> <span class="rating-bar"> <img src="{{asset('images/raing-star2.png')}}" alt="Rating Star" /> </span> */
/*                         <a href="{{path("smart_book_lecteur_DetailPub",{"id" :publications[i].getIdP()})}}" class="shop-btn">Plus information</a> */
/*                       <div class="cap-holder">*/
/*                         <p><img src="{{asset('images/image27.png')}}" alt="Best Choice" align="right"/>{{publications[i].getPresentation()}}</p>*/
/*                       </div>*/
/*                     </div>*/
/*                   </div>*/
/*                 </div>*/
/*               </div>*/
/*              {% endfor %}*/
/*                           */
/*             </div>*/
/*           </div>*/
/*           <nav class="bb-custom-nav"> <a href="#" id="bb-nav-prev" class="left-arrow">Previous</a> <a href="#" id="bb-nav-next" class="right-arrow">Next</a> </nav>*/
/*         </section>*/
/*         <span class="slider-bottom"><img src="{{asset('images/slider-bg.png')}}" alt="Shadow"/></span> </section>*/
/*       <section class="span12 wellcome-msg m-bottom first">*/
/*         <h2>Bienvenue chez SmartBook store.</h2>*/
/*         <p>Plateforme pour partage nos lectures et nos ecritures</p>*/
/*       </section>*/
/*     </section>*/
/*    */
/*     */
/*     <section class="row-fluid features-books">*/
/*       <section class="span12 m-bottom">*/
/*         <div class="heading-bar">*/
/*           <h2>Autres publications</h2>*/
/*           <span class="h-line"></span> </div>*/
/*           <center> <div class="slider1">*/
/*            {% for i in 0..publications|length-1 %}*/
/*           <div class="slide"> <a href="{{path("smart_book_lecteur_DetailPub",{"id" :publications[i].getIdP()})}}"><img src="{{ asset(['uploads/livres/',publications[i].getLivre().getImageL()]|join ) }}" alt="" class="pro-img"/>{{publications[i].getLivre().getNomLivre()}}</a> <span class="title"><a href="{{path("smart_book_lecteur_DetailPub",{"id" :publications[i].getIdP()})}}">{{publications[i].getLivre().getAuteur()}}</a></span> <span class="rating-bar"><img src="{{asset('images/rating-star.png')}}" alt="Rating Star"/></span>*/
/*             <div class="cart-price"> <a class="cart-btn2" href="">Add to Cart</a> <span class="price">{{publications[i].getLivre().getPrix()}} DT</span> </div>*/
/*           </div>*/
/*           {% endfor %}         */
/*         </div>*/
/*         </center>*/
/*       </section>*/
/*     </section>*/
/*  </section>*/
/* {% endblock %}*/
