<?php

/* SmartBookLecteurBundle:Livre:list_livres.html.twig */
class __TwigTemplate_9534d462835b77e886f8951ea54a574e56e1854d92f43682c8eb86ca341219d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:Livre:list_livres.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "    <section id=\"content-holder\" class=\"container-fluid container\">
    <section class=\"row-fluid\">
     
    \t<div class=\"heading-bar\">
        \t<h2>Livres</h2>         
           </div>
          ";
        // line 10
        if (((isset($context["livres"]) ? $context["livres"] : $this->getContext($context, "livres")) == null)) {
            // line 11
            echo "        <center><h1 class=\"titre_annonce\">Aucun Livre</h1> </center>
        ";
        } else {
            // line 13
            echo "          ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, (isset($context["livres"]) ? $context["livres"] : $this->getContext($context, "livres"))) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 14
                echo "          <section class=\"list-holder\">
            \t<article class=\"item-holder\">
                \t<div class=\"span2\">
                    \t<a href=\"book-detail.html\"><img src=\"";
                // line 17
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(twig_join_filter(array(0 => "uploads/livres/", 1 => $this->getAttribute($this->getAttribute((isset($context["livres"]) ? $context["livres"] : $this->getContext($context, "livres")), $context["i"], array(), "array"), "getImageL", array(), "method")))), "html", null, true);
                echo "\" alt=\"Image07\" /></a> </div>
                    <div class=\"span10\">
                    \t<div class=\"title-bar\"><a href=\"\">";
                // line 19
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["livres"]) ? $context["livres"] : $this->getContext($context, "livres")), $context["i"], array(), "array"), "getNomLivre", array(), "method"), "html", null, true);
                echo "</a></div>
                        <strong>Auteur</strong>
                        <p>";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["livres"]) ? $context["livres"] : $this->getContext($context, "livres")), $context["i"], array(), "array"), "getAuteur", array(), "method"), "html", null, true);
                echo "</p>
                        <strong>Langue</strong>
                        <p>";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["livres"]) ? $context["livres"] : $this->getContext($context, "livres")), $context["i"], array(), "array"), "getLangue", array(), "method"), "html", null, true);
                echo "</p>          
                        <strong>Date edition</strong>
                        <p>";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["livres"]) ? $context["livres"] : $this->getContext($context, "livres")), $context["i"], array(), "array"), "getDateEdition", array(), "method"), "html", null, true);
                echo "</p>
                        <strong>Nombre des pages</strong>
                        <p>";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["livres"]) ? $context["livres"] : $this->getContext($context, "livres")), $context["i"], array(), "array"), "getNbrPages", array(), "method"), "html", null, true);
                echo "</p>
                         <strong>Prix</strong>
                        <p>";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["livres"]) ? $context["livres"] : $this->getContext($context, "livres")), $context["i"], array(), "array"), "getPrix", array(), "method"), "html", null, true);
                echo "</p>
                         <a href=\"\" class=\"shop-btn\">Ses livres</a>
                        </div>
                    
                </article>
          </section>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo "        ";
        }
        // line 37
        echo "        </section>
        </section>
    ";
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:Livre:list_livres.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 37,  98 => 36,  85 => 29,  80 => 27,  75 => 25,  70 => 23,  65 => 21,  60 => 19,  55 => 17,  50 => 14,  45 => 13,  41 => 11,  39 => 10,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* {% block contenu %}*/
/*     <section id="content-holder" class="container-fluid container">*/
/*     <section class="row-fluid">*/
/*      */
/*     	<div class="heading-bar">*/
/*         	<h2>Livres</h2>         */
/*            </div>*/
/*           {% if livres ==null %}*/
/*         <center><h1 class="titre_annonce">Aucun Livre</h1> </center>*/
/*         {% else %}*/
/*           {% for i in 0..livres|length-1 %}*/
/*           <section class="list-holder">*/
/*             	<article class="item-holder">*/
/*                 	<div class="span2">*/
/*                     	<a href="book-detail.html"><img src="{{asset(['uploads/livres/',livres[i].getImageL()]|join )}}" alt="Image07" /></a> </div>*/
/*                     <div class="span10">*/
/*                     	<div class="title-bar"><a href="">{{livres[i].getNomLivre()}}</a></div>*/
/*                         <strong>Auteur</strong>*/
/*                         <p>{{livres[i].getAuteur()}}</p>*/
/*                         <strong>Langue</strong>*/
/*                         <p>{{livres[i].getLangue()}}</p>          */
/*                         <strong>Date edition</strong>*/
/*                         <p>{{livres[i].getDateEdition()}}</p>*/
/*                         <strong>Nombre des pages</strong>*/
/*                         <p>{{livres[i].getNbrPages()}}</p>*/
/*                          <strong>Prix</strong>*/
/*                         <p>{{livres[i].getPrix()}}</p>*/
/*                          <a href="" class="shop-btn">Ses livres</a>*/
/*                         </div>*/
/*                     */
/*                 </article>*/
/*           </section>*/
/*           {% endfor %}*/
/*         {% endif %}*/
/*         </section>*/
/*         </section>*/
/*     {% endblock %}*/
/* */
