<?php

/* SmartBookLecteurBundle:publication:gerer_publications.html.twig */
class __TwigTemplate_93bef30472407eed80dca74315894aeaf050072bfd9e6552b25cc9937d603e6f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("SmartBookLecteurBundle::layout2.html.twig", "SmartBookLecteurBundle:publication:gerer_publications.html.twig", 2);
        $this->blocks = array(
            'contenu' => array($this, 'block_contenu'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SmartBookLecteurBundle::layout2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_contenu($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if (((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")) == null)) {
            // line 5
            echo "        <center><h1 class=\"titre_annonce\">Aucune annonce est publié</h1> </center>
    ";
        } else {
            // line 7
            echo "    <center>
    <div id=\"div_titre\">
    <h1 class=\"titre_annonce\">Mes annonces</h1>
    </div>
     <table class=\"tab_pubs\" border=\"1\">
        <tr>
            <td class=\"td_pubs\">Titre</td>
            <td class=\"td_pubs\">Type </td>
            <td class=\"td_pubs\">Date annonce </td>
            <td class=\"td_pubs\">Texte Annonce</td>
            <td class=\"td_pubs\">Nom Livre </td>
            <td class=\"td_pubs\">Supprimer</td>
            <td class=\"td_pubs\">Modifier</td>            
        </tr>
       ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["publications"]) ? $context["publications"] : $this->getContext($context, "publications")));
            foreach ($context['_seq'] as $context["_key"] => $context["pub"]) {
                // line 22
                echo "           <tr>
               <td class=\"td_data\">";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($context["pub"], "getTitre", array(), "method"), "html", null, true);
                echo "</td> 
                <td class=\"td_data\">";
                // line 24
                echo twig_escape_filter($this->env, $this->getAttribute($context["pub"], "getType", array(), "method"), "html", null, true);
                echo "</td>  
                <td class=\"td_data\">";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($context["pub"], "getDatePub", array(), "method"), "html", null, true);
                echo "</td>  
                <td class=\"td_data\">";
                // line 26
                echo twig_escape_filter($this->env, $this->getAttribute($context["pub"], "getPresentation", array(), "method"), "html", null, true);
                echo "</td> 
                <td class=\"td_data\">";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pub"], "getLivre", array(), "method"), "getNomLivre", array(), "method"), "html", null, true);
                echo "</td> 
                <td class=\"td_data\"><a href=\"";
                // line 28
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_Sup_Pub", array("id" => $this->getAttribute($context["pub"], "getIdP", array(), "method"), "userid" => 38)), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/annuler.png"), "html", null, true);
                echo "\" alt=\"1\"></a></td> 
                <td class=\"td_data\"><a href=\"";
                // line 29
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("smart_book_lecteur_ModifPub", array("id" => $this->getAttribute($context["pub"], "getIdP", array(), "method"), "type" => $this->getAttribute($context["pub"], "getType", array(), "method"), "userid" => 38)), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/modifier.png"), "html", null, true);
                echo "\" alt=\"1\"></a></td>               
           </tr>
           
       ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pub'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "    </table>
    </center>
     ";
        }
    }

    public function getTemplateName()
    {
        return "SmartBookLecteurBundle:publication:gerer_publications.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 33,  87 => 29,  81 => 28,  77 => 27,  73 => 26,  69 => 25,  65 => 24,  61 => 23,  58 => 22,  54 => 21,  38 => 7,  34 => 5,  31 => 4,  28 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "SmartBookLecteurBundle::layout2.html.twig" %}*/
/* {% block contenu %}*/
/*     {% if publications ==null %}*/
/*         <center><h1 class="titre_annonce">Aucune annonce est publié</h1> </center>*/
/*     {% else %}*/
/*     <center>*/
/*     <div id="div_titre">*/
/*     <h1 class="titre_annonce">Mes annonces</h1>*/
/*     </div>*/
/*      <table class="tab_pubs" border="1">*/
/*         <tr>*/
/*             <td class="td_pubs">Titre</td>*/
/*             <td class="td_pubs">Type </td>*/
/*             <td class="td_pubs">Date annonce </td>*/
/*             <td class="td_pubs">Texte Annonce</td>*/
/*             <td class="td_pubs">Nom Livre </td>*/
/*             <td class="td_pubs">Supprimer</td>*/
/*             <td class="td_pubs">Modifier</td>            */
/*         </tr>*/
/*        {% for pub in publications %}*/
/*            <tr>*/
/*                <td class="td_data">{{pub.getTitre()}}</td> */
/*                 <td class="td_data">{{pub.getType()}}</td>  */
/*                 <td class="td_data">{{pub.getDatePub()}}</td>  */
/*                 <td class="td_data">{{pub.getPresentation()}}</td> */
/*                 <td class="td_data">{{pub.getLivre().getNomLivre()}}</td> */
/*                 <td class="td_data"><a href="{{path("smart_book_lecteur_Sup_Pub",{"id" :pub.getIdP(),"userid":38})}}"><img src="{{asset('images/annuler.png')}}" alt="1"></a></td> */
/*                 <td class="td_data"><a href="{{path("smart_book_lecteur_ModifPub",{"id" :pub.getIdP(),"type":pub.getType(),"userid":38})}}"><img src="{{asset('images/modifier.png')}}" alt="1"></a></td>               */
/*            </tr>*/
/*            */
/*        {% endfor %}*/
/*     </table>*/
/*     </center>*/
/*      {% endif %}*/
/* {% endblock %}*/
/* */
